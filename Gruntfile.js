'use strict';

module.exports = function (grunt) {
	require('load-grunt-tasks')(grunt);
	
	grunt.initConfig({
		env: {
			default: {
				PORT: 3000,
				DOMAIN: 'bln.local'
			}
		},
		
		express: {
			server: {
				options: {
					script: 'app/server.js'
				}
			}
		},
		// Add vendor prefixed styles
		autoprefixer: {
			options: {
				browsers: ['last 2 version', 'ie 8', 'ie 9'],
				diff: true
			},
			single_file: {
				files: [{
					expand: true,
					cwd: 'public/**/',
					src: '{,*/}*.scss',
					dest: 'public/**/'
				}]
			}
		},
		
		// Watches files and folders
		watch: {
			// change to this gruntfile
			gruntfile: {
				files: 'Gruntfile.js'
			},
			
			html: {
				files: 'public/**/*.html',
				options: {
					livereload: 35728 // bln-web has livereload on default port, must be different
				}
			},
			
			js: {
				files: [
					'public/**/{,*/}*.{json,js}',
					'integrations/**/*.js'
				],
				// tasks: [],
				options: {
					livereload: 35728
				}
			},
			
			// css: {
			// 	files: 'public/**/{,*/}*.{scss,sass}',
			// 	tasks: ['sass', 'autoprefixer'],
			// 	options: {
			// 		livereload: 35728
			// 	}
			// }
		},
		
		// code style checks
		tabs4life: {
			app: {src: 'app/**/*.js'},
			grunt: {src: 'Gruntfile.js'},
			integration: {src: 'integrations/dreambuilder/*.js'}
		}
	});
	
	// Server
	grunt.registerTask('server', ['env', 'express:server', 'autoprefixer', 'watch']);
	
	// Initiate
	grunt.registerTask('default', ['server']);
};
