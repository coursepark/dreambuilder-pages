var when = require('when');
var nodefn = require('when/node');
var request = require('request');
var express = require('express');
var app = express();

var testfactory = {
	apiUrl: 'http://api.bluedrop.thetestfactory.com/service.asmx/GenerateUrl_AnswerEditor',
	applicationKey: '8f245a35-c27c-4e85-8d08-3163ead87fe9',
	baseUrl: 'http://app.bluedrop.thetestfactory.com'
};
var blnApiEnv = process.env.BLN_API;
var xapiURL = process.env.XAPI_URL;
var coreURL = process.env.CORE_URL;
var siteURL = process.env.SITE_URL;
//////////////////////////////////////////////////
var blnToken;
var blnOrgId = 10001;
var clProgram = 10095;
var peProgram = 10096;
var enProgram = 10094;
var enCapProgram = 10146;
var esCapProgram = 10319;
var enFreeProgram = 10104;
var peFreeProgram = 10106;
//////////////////////////////////////////////////
var clProduct = 10112;
var peProduct = 10113;
var enProduct = 10111;
var peFreeProduct = 10133;
var clFreeProduct = 10132;
var enFreeProduct = 10131;
var TFResult;
//////////////////////////////////////////////////
var cors = require('cors');
var corsWhitelist = [
	'https://dreambuilder.bluedrop.io',
	'http://dreambuilder.bluedrop.io'
];
var corsOptions = {
	origin: function (origin, callback) {
		var originIsWhitelisted = corsWhitelist.indexOf(origin) !== -1;
		callback(null, originIsWhitelisted);
	}
};
//BLN AUTH-----------------------------------------------------------------
function blnAuth(blnOrgId) {
	var apiurl = blnApiEnv;
	var data = {
		url: apiurl + '/auth/login/',
		headers: {
			"Origin": "bluedrop.io",
		},
		contentType: "application/json",
		json: true,
		body: {
			"organizationId": 10001,
			"username": "app_account+dreambuilder@bluedrop360.com",
			"password": "GgVvdzdj"
		}
	};
	return nodefn.lift(request.post)(data)
		.then(function (result) {
			body = result[1];
			blnToken = 'Bearer ' + body.accessToken;
			return blnToken;
		});
};

function userAuth() {
	blnToken = localStorage.getItem("blnp.accessToken");
	return blnToken
}
///////////////////////////////////////////////////////////////////////////////////////////
app.get('/tools-redirect', function (req, res) {
	var uid = req.query.userId;
	blnAuth()
		.then(function (blnToken) {
			userProductLookup(blnToken, uid, 'products')
				.then(function (result) {
					switch (result) {
						case clProduct:
						case clFreeProduct:
							res.redirect(303, '/resources/Chile/index.html');
							break;
						case enProduct:
						case enFreeProduct:
							res.redirect(303, '/resources/USA/index.html');
							break;
						case peProduct:
						case peFreeProduct:
							res.redirect(303, '/resources/Peru/index.html');
							break;
						default:
							throw new Error("You do not have access.");
							break;
					}
				});
		});
});
//USER PRODUCT LOOKUP-------------------------------------------------------------
function userProductLookup(token, uid, target) {
	var apiurl = blnApiEnv;
	return nodefn.lift(request.get)({
			url: apiurl + '/users/' + uid + '/' + target,
			'content-type': 'application/json',
			json: true,
			headers: {
				"Authorization": token
			}
		})
		.then(function (result) {
			switch (target) {
				case 'products':
					body = result[1];
					console.log("LOOKUP: -------");
					console.log(body);
					console.log("----------");
					for (i = 0; i < body.length; i++) {
						if (body[i].type == "newprogram" || body[i].type == "program") {
							if (!body[i].id) {
								throw new Error("You do not have access.");
							} else {
								obj = body[i].id;
								i = body.length;
							}
						}
					}
					break;
				default:
					body = result[1];
					obj = body;
					break;
			}
			return obj;
		});
}
//PRODUCT LOOKUP-------------------------------------------------------------
function programLookup(token, p) {
	var apiurl = blnApiEnv;
	switch (p) {
		case "clProgram":
			pid = 10095;
			break;
		case "peProgram":
			pid = 10096;
			break;
		case "enProgram":
			pid = 10094;
			break;
		case "enCapProgram":
			pid = 10146;
			break;
		case "esCapProgram":
			pid = 10319;
			break;
		case "enFreeProgram":
			pid = 10104;
			break;
		case "peFreeProgram":
			pid = 10106;
			break;
		default:
			pid = p;

	}
	return nodefn.lift(request.get)({
			'url': xapiURL + "/programs/" + pid,
			'content-type': 'application/json',
			'json': true,
			'headers': {
				'Authorization': token
			}
		})
		.then(function (result) {
			body = result[1];
			if (!body.id) {
				throw new Error("You do not have access.");
			} else {
				productID = body.productId;
				return productID;
			}
		});
}
//---------------------------------------------------------------------
app.use(require('body-parser').json());
//GET ENV VARS---------------------------------------------
var env;
app.options('/env', cors());
app.post('/env', cors(corsOptions), function (req, res) {
	env = {
		"siteURL": siteURL,
		"coreURL": coreURL,
		"apiURL": blnApiEnv,
		"xapIURL": xapiURL
	}
	res.status(202).send(env)
});
//-----------------------------------------------------------------
//-CHECK USER ACCOUNT----------------------------------------------
app.options('/userLookupRedirect', cors());
app.post('/userLookupRedirect', cors(corsOptions), function (req, res) {
	var user = {
		token: req.body.token,
		url: req.body.url
	};
	if (user.url && user.url === "support") {
		var apiurl = 'https://kube-support.bluedrop360.com/api-v1';
	} else {
		var apiurl = blnApiEnv;
	}
	return nodefn.lift(request.get)({
			url: apiurl + '/user',
			'content-type': 'application/json',
			json: true,
			headers: {
				"Authorization": user.token
			}
		})
		.then(function (result) {
			body = result[1];
			var username = body.username;
			var results;
			if (body.id) {
					userProductLookup(user.token, body.id, 'products')
						.then(function (result) {
							if (result == enProduct || result == enFreeProduct) {
								loc = 'en';
								lid = 4;
								return res.status(200).json({"redirect": "/businessPlan/en/"});
							} else {
								loc = 'es';
								lid = 2;
								return res.status(200).json({"redirect": "/businessPlan/es/"});
							}
						});
			} else {
				results = {
					status:404,
					error: "unmatched user"
				}
				res.status(202).send(results);
			}
		});
});
//CREATE ACCOUNT-------------------------------------------------------
app.options('/signup', cors());
app.post('/signup', cors(corsOptions), function (req, res) {
	var apiurl = blnApiEnv;
	var blnorgid = blnOrgId;
	var newUser = {
		'user': req.body.user,
		'otherInfo': req.body.otherInfo,
		'termsOfServiceAccepted': req.body.env.timestamp,
		'returnUrl': req.body.env.returnUrl,
		'channelId': req.body.env.channelId
	};
	blnAuth()
		.then(function (blnToken) {
			return nodefn.lift(request.post)({
				'url': apiurl + '/organizations/' + blnorgid + '/new-user',
				'content-type': 'application/json',
				'body': newUser,
				'json': true,
				'headers': {
					'Authorization': blnToken
				}
			})
		})
		.then(function (result) {
			var response = result[0],
				body = result[1];
			if (response.statusCode < 200 || response.statusCode > 299) {
				throw new Error(response.statusCode + " | " + body.message);
			}
			res.status(202).send(body);
		})
		.catch(function (e) {
			res.status(400).send(e.message);
		})
		.done();

});
//LOG INTO ACCOUNT-------------------------------------------------------
app.options('/cplogin', cors());
app.post('/cplogin', cors(corsOptions), function (req, res) {

	var userEmail = req.body.email;
	var userPass = req.body.pass;

	return nodefn.lift(request.get)({
			'url': 'https://www.coursepark.com/api/account/?email=' + encodeURIComponent(userEmail) + '&password=' + userPass,
			'contentType': "application/json",
		})
		.then(function (result) {
			var body = result[1];
			res.status(202).send(body);
		});
});
//UPDATE PASSWORD-------------------------------------------------------
app.options('/updatePass', cors());
app.post('/updatePass', cors(corsOptions), function (req, res) {
	var apiurl = blnApiEnv;
	var userDetails = {
		'termsOfServiceAccepted': req.body.timestamp,
		'email': req.body.email,
		'password': req.body.pass
	}
	blnAuth()
		.then(function (blnToken) {
			return nodefn.lift(request.get)({
				'url': apiurl + '/users/' + userDetails.email,
				'content-type': 'application/json',
				'json': true,
				'headers': {
					'Authorization': blnToken
				}
			})
		})
		.then(function (result) {
			var response = result[0],
				body = result[1];
			var uid = body.id;
			if (uid) {
				var updateTermsObj = {
					"termsOfServiceAccepted": userDetails.termsOfServiceAccepted
				};
				return nodefn.lift(request.put)({
						'url': apiurl + '/organizations/' + blnOrgId + '/users/' + uid,
						'content-type': 'application/json',
						'json': true,
						'body': updateTermsObj,
						'headers': {
							'Authorization': blnToken
						}
					})
					.then(function (result) {
						var updatePassObj = {
							"newPassword": userDetails.password,
							"organizationIdOrKey": 10001
						};
						return nodefn.lift(request.put)({
								'url': apiurl + '/users/' + uid + '/password/',
								'content-type': 'application/json',
								'json': true,
								'body': updatePassObj,
								'headers': {
									'Authorization': blnToken
								}
							})
							.then(function (result) {
								response = result[0];
								res.status(202).send(response);
								if (response.statusCode < 200 || response.statusCode > 299) {
									throw new Error('Could update password. ' + response.statusCode);
								}
							})
							.catch(function (e) {
								res.status(400).send(e);
							})
					})
			} else {
				res.status(400).send(response);
			}
		})
		.catch(function (e) {
			res.status(400).send(e.message);
		})
		.done();
});
//NETWORK JOIN -----------------------------------------------------------
app.options('/networkjoin', cors());
app.post('/networkjoin', cors(corsOptions), function (req, res) {
	var apiurl = blnApiEnv;
	var blnorgid = blnOrgId;
	var userItems = {
		"userId": req.body.user.userId,
		"networkId": req.body.user.networkId,
		"program": req.body.user.program
	}

	var newUserNetwork = {
		"role": "member"
	};
	blnAuth()
		.then(function (blnToken) {
			return nodefn.lift(request.put)({
				'url': apiurl + '/organizations/' + blnOrgId + '/networks/' + userItems.networkId + '/users/' + userItems.userId,
				'content-type': 'application/json',
				'body': newUserNetwork,
				'json': true,
				'headers': {
					'Authorization': blnToken
				}
			})
		})
		.then(function (result) {
			var response = result[0],
				body = result[1];
			if (response.statusCode < 200 || response.statusCode > 299) {
				throw new Error('Could not join network. ' + body.message);
			}
			programLookup(blnToken, userItems.program)
				.then(function (result) {
					return nodefn.lift(request.put)({
							'url': apiurl + '/users/' + userItems.userId + '/products/' + result,
							'content-type': 'application/json',
							'body': newUserNetwork,
							'json': true,
							'headers': {
								'Authorization': blnToken
							}
						})
						.then(function (result) {
							var response = result[0],
								body = result[1];
							if (response.statusCode < 200 || response.statusCode > 299) {
								throw new Error('Could not assign product. ' + body.message);
							}
							var secondaryProgram = false;;
							if (userItems.program == "enProgram") {
								secondaryProgram = "enCapProgram";
							}
							if (userItems.program == "esProgram" || userItems.program == "clProgram" || userItems.program == "peProgram") {
								secondaryProgram = "esCapProgram";
							}
							if (secondaryProgram) {
								programLookup(blnToken, secondaryProgram)
									.then(function (result) {
										return nodefn.lift(request.put)({
												'url': apiurl + '/users/' + userItems.userId + '/products/' + result,
												'content-type': 'application/json',
												'body': newUserNetwork,
												'json': true,
												'headers': {
													'Authorization': blnToken
												}
											})
											.then(function (result) {
												var response = result[0],
													body = result[1];
												res.status(202).send(body);
											})
									})
							} else {
								res.status(202).send(body);
							}
						})
				});
		})
		.catch(function (e) {
			res.status(400).send(e.message);
		})
		.done();
});
/////////////////////////////////////////////////////////////////////
var client;
app.options('/clientLookup', cors());
app.post('/clientLookup', cors(corsOptions), function (req, res) {
	client = clientSettings[req.body.client];
	res.status(202).send(client)
});
/////////////////////////////////////////////////////////////////////
app.options('/clientListLookup', cors());
app.post('/clientListLookup', cors(corsOptions), function (req, res) {
	res.status(202).send(clientSettings)
});

/////////////////////////////////////////////////////////////////////
app.options('/networkLookup', cors());
app.post('/networkLookup', cors(corsOptions), function (req, res) {
	blnAuth()
		.then(function (blnToken) {
			return nodefn.lift(request.get)({
				'url': blnApiEnv + '/networks?organizationId=' + blnOrgId + "&query=" + req.body.name,
				'content-type': 'application/json',
				'json': true,
				'headers': {
					'Authorization': blnToken
				}
			})
		})
		.then(function (result) {
			body = result[1];
			res.status(202).send(body);
		});
});
/////////////////////////////////////////////////////////////////////
app.options('/channelLookup', cors());
app.post('/channelLookup', cors(corsOptions), function (req, res) {
	blnAuth()
		.then(function (blnToken) {
			return nodefn.lift(request.get)({
					'url': blnApiEnv + '/channels?organizationId=' + blnOrgId + "&id=" + req.body.id,
					'content-type': 'application/json',
					'json': true,
					'headers': {
						'Authorization': blnToken
					}
				})
				.then(function (result) {
					chan = result[1];
					res.status(202).send(chan);
				})
		})
});
//TOOLS REDIRECT-----------------------------------------------------------
app.options('/userProgram', cors());
app.post('/userProgram', cors(corsOptions), function (req, res) {
	var uid = req.body.userId;
	blnAuth()
		.then(function (blnToken) {
			userProductLookup(blnToken, uid, 'products')
				.then(function (result) {
					prod = {};
					switch (result) {
						case clProduct:
							prod["item"] = clProgram;
							break;
						case peProduct:
							prod["item"] = peProgram;
							break;
						case enProduct:
							prod["item"] = enProgram;
							break;
						case enFreeProduct:
							prod["item"] = enFreeProgram;
							break;
						case peFreeProduct:
							prod["item"] = peFreeProgram;
							break;
					}
					res.status(200).send(prod);
				});
		});
});
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
var clientSettings = {
	"candelaria": {
		"name": "Candelaria",
		"channame": "DreamBuilder (Candelaria)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["es"],
		"en-program_id": "enProgram",
		"es-program_id": "clProgram",
		"logo": "",
		"country": "CL",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": "standard",
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"aweec": {
		"name": "Arizona Women's Education and Entrepreneur Center",
		"channame": "Arizona Women’s Education and Entrepreneur Center",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": "",
		"network_select": {
			"languageStrings": {
				"en": {
					"network_select": "Please select..."
				},
				"es": {
					"network_select": "Seleccione su país ..."
				}
			},
			"alerts": {
				"en": {
					"network": "Please enter your country before continuing."
				},
				"es": {
					"network": "Por favor, introduzca su país antes de continuar."
				}
			}
		}
	},
	"perucamaras": {
		"name": "Peru Camaras and CCL",
		"channame": "DreamBuilder (Peru Camaras and CCL)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "PE",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"chile": {
		"name": "Dreambuilder Chile",
		"channame": "Dreambuilder (Chile)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["es"],
		"en-program_id": "enProgram",
		"es-program_id": "clProgram",
		"logo": "",
		"country": "CL",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": "standard",
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"elabra": {
		"name": "El Abra",
		"channame": "DreamBuilder (El Abra)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["es"],
		"en-program_id": "enProgram",
		"es-program_id": "clProgram",
		"logo": "",
		"country": "CL",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": "standard",
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"prodemu": {
		"name": "PROdeMU Antofagasta",
		"channame": "PROdeMU Antofagasta",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["es"],
		"en-program_id": "enProgram",
		"es-program_id": "clProgram",
		"logo": "",
		"country": "CL",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": "standard",
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": "",
		"index":"prodemuSelector"
	},
	"bocnet": {
		"name": "Business Outreach Center WBC",
		"channame": "DreamBuilder ( Business Outreach Center)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"fitwbc": {
		"name": "weVENTURE Florida Institute of Technology",
		"channame": "DreamBuilder ( weVENTURE - FIT )",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"freshstart": {
		"name": "Fresh Start",
		"channame": "DreamBuilder (Fresh Start)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"mac-sa": {
		"name": "WBC of Southern Arizona",
		"channame": "DreamBuilder (WBC of Southern Arizona)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"mcewbc": {
		"name": "WBC Maryland Capital Enterprises",
		"channame": "DreamBuilder (MCE WBC)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"mcrc": {
		"name": "WBC of Mi Casa Resource Center",
		"channame": "DreamBuilder ( Mi Casa)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"nevadawbc": {
		"name": "Nevada WBC",
		"channame": "Nevada Women’s Business Center",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"wbccornerstone": {
		"name": "WBC at Cornerstone Alliance",
		"channame": "DreamBuilder (WBC at Cornerstone Alliance)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"wbcidaho": {
		"name": "Uni. Of Idaho",
		"channame": "Uni. Of Idaho",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg", "lj-sourcing-logo.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"uidaho": {
		"name": "Uni. Of Idaho",
		"channame": "Uni. Of Idaho",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg", "lj-sourcing-logo.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"wbc-inw": {
		"name": "WBC Inland Northwest (SNAP)",
		"channame": "DreamBuilder (WBC INW)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg", "sp-logo-sba.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"wbcodubg": {
		"name": "WBC of Old Dominion University",
		"channame": "DreamBuilder (WBC at ODU Business Gateway)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg", "hrhcc.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"wbcohio": {
		"name": "WBC of ECDI",
		"channame": "DreamBuilder (The Economic Community Development Institute)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"wbcral": {
		"name": "WBC Rural Business Initiative of Alabama",
		"channame": "Women’s Business Center Rural Business Initiative",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"wbcrgvxx": {
		"name": "WBC of Rio Grande Valley",
		"channame": "DreamBuilder ( Rio Grande Valley’s WBC)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"wbcsa": {
		"name": "WBC of South Alabama",
		"channame": "DreamBuilder (WBC of South Alabama)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"wesst": {
		"name": "WESST WBC",
		"channame": "DreamBuilder (WESST Enterprise Center)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"westcompany": {
		"name": "WBC West Company California",
		"channame": "DreamBuilder (West Company)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"wwbic": {
		"name": "WBC at Wisconsin Women's Business Initiative ",
		"channame": "DreamBuilder (Wisconsin Women’s Business Initiative Corporation)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"cacapital": {
		"name": "WBC of California Capital",
		"channame": "DreamBuilder (California Capital WBC)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"ices": {
		"name": "WBC of Iowa Center for Economic Success",
		"channame": "Dreambuilder (Iowa Center for Economic Success)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"wbcodubg-spa": {
		"name": "WBC of Old Dominion University - Spanish",
		"channame": "DreamBuilder (WBC at ODU Business Gateway - SPA)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg", "hrhcc.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"mink": {
		"name": "Patsy T. Mink Center for Business & Leadership",
		"channame": "DreamBuilder (Patsy T. Mink Center for Business & Leadership)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"wisecenter": {
		"name": "WISE WBC at Syracuse University ",
		"channame": "WISE Women's Business Center DreamBuilder",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"sba": {
		"name": "DreamBuilder SBA",
		"channame": "DreamBuilder (SBA)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": true,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"emprendamos": {
		"name": "Fundación Emprendamos Ya.com",
		"channame": "Fundación Emprendamos Ya.com",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "CO",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": "standard",
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"mara": {
		"name": "Mara Foundation",
		"channame": "DreamBuilder (Mara Foundation)",
		"network": 42307,
		"channel_id": 10031,
		"include_langs": ["en"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "CO",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": "gender",
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": "",
		"index": "maraSelector"
	},
	"hualapai": {
		"name": "Hualapai",
		"channame": "DreamBuilder (Hualapai)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"tonation": {
		"name": "Tohono O'odham",
		"channame": "DreamBuilder (Tohono O'odham)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"sancarlos": {
		"name": "San Carlos Apache Tribe",
		"channame": "DreamBuilder (San Carlos Apache Tribe)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"wmat": {
		"name": "White Mountain Tribe",
		"channame": "DreamBuilder (White Mountain Tribe)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"dreamcatcher": {
		"name": "DreamCatcher",
		"channame": "DreamBuilder (DreamCatcher)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"regionaqp": {
		"name": "Gobierno Regional de Arequipa",
		"channame": "Dreambuilder (Gobierno Regional de Arequipa)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "PE",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": "standard",
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"centroschile-sercotec-bak": {
		"name": "Sercotec Chile",
		"channame": "Dreambuilder (Sercotec Chile)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["es"],
		"en-program_id": "enProgram",
		"es-program_id": "clProgram",
		"logo": "",
		"country": "CL",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": "standard",
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"wbdc": {
		"name": "Women's Business Development Center-Chicago",
		"channame": "Dreambuilder (Women's Business Development Center)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"mx": {
		"name": "Equal Futures Partnership in Mexico",
		"channame": "Dreambuilder (Equal Futures Partnership in Mexico)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "MX",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"banco": {
		"name": "Banco del Estudiante",
		"channame": "DreamBuilder (Banco del Estudiante)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "PE",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": "standard",
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"cerroverde": {
		"name": "Cerro Verde",
		"channame": "DreamBuilder (Cerro Verde)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "PE",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": "standard",
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"cid": {
		"name": "CID",
		"channame": "DreamBuilder (CID)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "PE",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": "standard",
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"hoop": {
		"name": "HOOP",
		"channame": "DreamBuilder (HOOP)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "PE",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": "standard",
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"peru": {
		"name": "DreamBuilder Peru",
		"channame": "DreamBuilder (Peru)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "PE",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": "standard",
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"promujer": {
		"name": "ProMujer",
		"channame": "ProMujer",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "MX",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"salta": {
		"name": "Salta Mentoring",
		"channame": "DreamBuilder (Salta Mentoring)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "PE",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": "standard",
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"theiowacenter": {
		"name": "WBC of Iowa Center for Economic Success",
		"channame": "Dreambuilder (Iowa Center for Economic Success)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"wbcywcasoaz": {
		"name": "WBC of Southern Arizona",
		"channame": "DreamBuilder (WBC of Southern Arizona)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"weventure": {
		"name": "weVENTURE Florida Institute of Technology",
		"channame": "DreamBuilder ( weVENTURE - FIT )",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"phoenixpubliclibrary": {
		"name": "Phoenix Public Library",
		"channame": "Phoenix Public Library",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": ["states", "library"],
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"freeport": {
		"name": "DreamBuilder Freeport-McMoRan ",
		"channame": "DreamBuilder Freeport-McMoRan ",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enFreeProgram",
		"es-program_id": "peFreeProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"wbcde": {
		"name": "WBC at First State Community Loan Fund",
		"channame": "Women's Business Center",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"prodemuatacama": {
		"name": "PROdeMU Atacama",
		"channame": "PROdeMU Atacama",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["es"],
		"en-program_id": "enProgram",
		"es-program_id": "clProgram",
		"logo": "",
		"country": "CL",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": "standard",
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": "",
		"redirect": "prodemu"
	},
	"sernamantofagasta": {
		"name": "Sernam Antofagasta",
		"channame": "DreamBuilder (Sernam Antofagasta)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["es"],
		"en-program_id": "enProgram",
		"es-program_id": "clProgram",
		"logo": "",
		"country": "CL",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": "standard",
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"sernamatacama": {
		"name": "Sernam Atacama",
		"channame": "Sernam Atacama",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["es"],
		"en-program_id": "enProgram",
		"es-program_id": "clProgram",
		"logo": "",
		"country": "CL",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": "standard",
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"sernam": {
		"name": "Sernam Parent",
		"channame": "DreamBuilder (Sernam)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["es"],
		"en-program_id": "enProgram",
		"es-program_id": "clProgram",
		"logo": "",
		"country": "CL",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": "standard",
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": "",
		"index": "sernamSelector"
	},
	"program": {
		"name": "DreamBuilder English",
		"channame": "DreamBuilder (English)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "/_client/images/dreambuilder-logo.png",
		"country": null,
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": "standard",
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-program.html",
		"styles": "db-program.css",
		"index": "programSelector"
	},
	"hartford-wbc": {
		"name": "WBC of the University of Hartford",
		"channame": "Hartford Edu",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"centroschile-sercotec": {
		"name": "Sercotec Chile",
		"channame": "Dreambuilder (Sercotec Chile)",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["es"],
		"en-program_id": "enProgram",
		"es-program_id": "clProgram",
		"logo": "",
		"country": "CL",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": "standard",
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": "",
		"index": "sercotecSelector"
	},
	"acewbc": {
		"name": "WBC Access to Capital for Entrepreneurs",
		"channame": "ACE WBC",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": "",
	},
	"wdeoc": {
		"name": "WBC Western Dairyland",
		"channame": "Western Dairyland EOC",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": "",
	},
	"americanspaces": {
		"name": "U.S. Department of State",
		"channame": "U.S. Department of State",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general-as.html",
		"styles": "",
		"index": "amspacesSelector"
	},
	"amep": {
		"name": "AMEP",
		"channame": "AMEP",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "PE",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"dreamcatcher2017": {
		"name": "DreamCatcher 2017",
		"channame": "DreamCatcher 2017",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"uwyc": {
		"name": "United Way of Yavapai County",
		"channame": "United Way of Yavapai County",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	
	"usembassyperu": {
		"name": "Embajadausa-peru",
		"channame": "Embajadausa-peru",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "PE",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": "standard",
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"wbbc": {
		"name": "Women's Business Border Center of the EPHCC",
		"channame": "Women's Business Border Center of the EPHCC",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"laedawbc": {
		"name": "LAEDA Women's Business Centre",
		"channame": "LAEDA Women's Business Centre",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"wbc-cc": {
		"name": "WBC-CC at the Rio Grande Valley",
		"channame": "DreamBuilder ( Rio Grande Valley’s WBC)",
		"network": 0,
		"channel_id": 10158,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"esp": {
		"name": "MVS",
		"channame": "MVS",
		"network": 0,
		"channel_id": 10158,
		"include_langs": ["es", "en"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": "",
		"index": "mvsSelector"
	},
	"mvsus": {
		"name": "MVS",
		"channame": "MVS",
		"network": 0,
		"channel_id": 10158,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": "states",
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"mvspe": {
		"name": "MVS",
		"channame": "MVS",
		"network": 0,
		"channel_id": 10158,
		"include_langs": ["es", "en"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "PE",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"mvscl": {
		"name": "MVS",
		"channame": "MVS",
		"network": 0,
		"channel_id": 10158,
		"include_langs": ["es", "en"],
		"en-program_id": "enProgram",
		"es-program_id": "clProgram",
		"logo": "",
		"country": "CL",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"localfirstaz": {
		"name": "Local First",
		"channame": "localfirst",
		"network": 0,
		"channel_id": 10158,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"salamovil": {
		"name": "Sala Móvil",
		"channame": "salamovil",
		"network": 0,
		"channel_id": 10158,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "PE",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": "standard",
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": ""
	},
	"wbcutah": {
		"name": "Northern WBCUtah Client",
		"channame": "northernwbcutahclient",
		"network": 0,
		"channel_id": 10158,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport-chase.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": "",
		"index": "utahSelector"
	},
	"womensbusinesscentre": {
		"name": "Tampa Bay Women's Business Centre",
		"channame": "tampabaywomensbusinesscentre",
		"network": 0,
		"channel_id": 10158,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["WBC_Logo_horizontal.png"],
		"member_filters": false,
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": "",
		"index": ""
	},
	"awe": {
		"name": "AWE Public",
		"channame": "awepublic",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["en", "es"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": "",
		"index": "aweSelector"
	},
	"redefe": {
		"name": "Cenpromype",
		"channame": "cenpromype",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["es", "en"],
		"en-program_id": "enProgram",
		"es-program_id": "peProgram",
		"logo": "",
		"country": "US",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": false,
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": "",
		"index": "cenproSelector"
	},
	"sala-dev": {
		"name": "Sala",
		"channame": "Sala",
		"network": 0,
		"channel_id": 0,
		"include_langs": ["es"],
		"en-program_id": "enProgram",
		"es-program_id": "clProgram",
		"logo": "",
		"country": "CL",
		"private_profile": false,
		"extLink": "",
		"footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
		"member_filters": "standard",
		"region_filter": false,
		"services": ["login", "reg"],
		"template": "template-general.html",
		"styles": "",
	}
};
module.exports = app;
