'use strict';

var expect = require('chai').expect;
var request = require('supertest');

describe('App', function () {
	var app, result;
	
	beforeEach(function () {
		result = undefined;
		process.env.DOMAIN = 'test.com';
		app = require('../app/index')();
	});
	
	describe('when requesting html file for organization', function () {
		beforeEach(function () {
			result = request(app)
				.get('/index.html')
				.set('Host', 'example.test.com')
			;
		});
		
		it('should return static file for that organization', function (done) {
			result.end(function (err, res) {
				expect(res.status).to.equal(200);
				expect(res.text).to.contain('<title>Example</title>');
				done();
			});
		});
	});
	
	describe('when requesting root for organization', function () {
		beforeEach(function () {
			result = request(app)
				.get('/')
				.set('Host', 'example.test.com')
			;
		});
		
		it('should return index.html for that organization', function (done) {
			result.end(function (err, res) {
				expect(res.status).to.equal(200);
				expect(res.text).to.contain('<title>Example</title>');
				done();
			});
		});
	});
	
	describe('when requesting file that does not exist for organization', function () {
		beforeEach(function () {
			result = request(app)
				.get('/wtf.html')
				.set('Host', 'example.test.com')
			;
		});
		
		it('should return index.html to support single page applications', function (done) {
			result.end(function (err, res) {
				expect(res.status).to.equal(200);
				expect(res.text).to.contain('<title>Example</title>');
				done();
			});
		});
	});
	
	describe('when trying to traverse directory structure using url', function () {
		beforeEach(function () {
			result = request(app)
				.get('/../../index.js')
				.set('Host', 'example.test.com')
			;
		});
		
		it('should respond with forbidden', function (done) {
			result.end(function (err, res) {
				expect(res.status).to.equal(403);
				expect(res.text).to.equal('Forbidden');
				done();
			});
		});
	});
	
	describe('when trying to traverse directory structure using host header', function () {
		beforeEach(function () {
			result = request(app)
				.get('/index.js')
				.set('Host', '/../../.test.com')
			;
		});
		
		it('should respond with not found', function (done) {
			result.end(function (err, res) {
				expect(res.status).to.equal(404);
				expect(res.text).to.contain('Cannot GET /index.js');
				done();
			});
		});
	});
	
	describe('when trying multiple sub domains', function () {
		beforeEach(function () {
			process.env.DOMAIN = 'subdomain.test.com';
			result = request(app)
				.get('/index.html')
				.set('Host', 'example.subdomain.test.com')
			;
		});
		
		it('should respond using lowest level domain as organization', function (done) {
			result.end(function (err, res) {
				expect(res.status).to.equal(200);
				expect(res.text).to.contain('<title>Example</title>');
				done();
			});
		});
	});
	
	describe('when requesting from no cache-control server', function () {
		beforeEach(function () {
			result = request(app)
				.get('/index.html')
				.set('Host', 'example.test.com')
			;
		});
		
		it('response should have no cache control', function (done) {
			result.end(function (err, res) {
				expect(res.get('Cache-Control') === 'max-age=0');
				done();
			});
		});
	});
	
	describe('when requesting from no cache-control environment server', function () {
		beforeEach(function () {
			process.env.CACHE_CONTROL_MAX_AGE = 3600;
			result = request(app)
				.get('/index.html')
				.set('Host', 'example.test.com')
			;
		});
		
		it('response should have cache control', function (done) {
			result.end(function (err, res) {
				expect(res.get('Cache-Control') === 'max-age=3600');
				done();
			});
		});
	});
});