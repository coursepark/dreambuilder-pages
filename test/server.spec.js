'use strict';

var expect = require('chai').expect;

describe('Server', function () {
	var result;
	
	beforeEach(function () {
		result = undefined;
	});
	
	describe('when starting server', function () {
		beforeEach(function () {
			process.env.DOMAIN = 'test.com';
			process.env.PORT = 8888;
			result = require('../app/server.js');
		});
		
		it('should start without error', function () {
			expect(result).to.be.ok;
		});
	});
});