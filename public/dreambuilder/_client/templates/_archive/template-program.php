<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>DreamBuilder - Bluedrop Learning Networks</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Raleway">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link href="/program/css/db-program.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-2.1.3.min.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="../db-unified/js/functions.js" type="text/javascript"></script>
    <script src="../db-unified/js/jquery.autocomplete.min.js" type="text/javascript"></script>
    <script>
            // INIT values -----------------------------------
    var defaultLang = "<?php
    echo $default_lang ?>"; // Default language for page
    var include_langs = "<?php
    echo $include_langs ?>";  // languages for page
    var current = "<?php
    echo $current ?>"
    var network = <?php
    echo $network ?>;
    var footerlen = <?php
    echo $footerlen ?>;
    var logo = "<?php echo $logo ?>";
    var CLForm = 1;
    var country;
    console.log("Referrer: "+document.referrer);
    console.log("Domain: "+document.domain);
    //-------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------
    </script>
</head>
<body class="program-body">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 program-logo" style="text-align:center"><img src="images/dreambuilder-logo.png"></div>
            <div class="col-xs-12">
                <div class="form-block form-block-program col-xs-8 col-sm-8 col-md-8 col-lg-8 col-lg-offset-2 col-md-offset-2 col-md-offset-2">
                    <div class="col-xs-12 regswitch"><ul class="nav nav-tabs">
                        <li class="formtitle programformtitle" id="formtitle"></li>
                        <li id ="tab_reg" role="presentation" class="active"><a href="javascript:switchTab('reg');"><span id="navregister"></span></a></li>
                        <li id="tab_login" role="presentation"><a href="javascript:switchTab('login');"><span id="navlogin"></span></a></li>
                    </ul></div>
                    <!--------------------------------------------------------------------->
                    <!--<h2 align="center"><span class="whiteText">Not a Member? Register Now!</span></h2>-->
                    <div id="regloginform" class=" col-xs-12 formborder" >
                        <div class="form-group col-xs-12">
                            <div id="languageSelectDropdown"></div>
                        </div>
                        <div id="regform">
                            <div class="form-group col-xs-6">
                                <label id="signupfirstname"></label>
                                <input type="text" name="fName" id="fName" class="form-control">
                            </div>
                            <div class="form-group col-xs-6">
                                <label id="signuplastname"></label>
                                <input type="text" name="lName" id="lName" class="form-control">
                            </div>
                            <div class="form-group col-xs-12">
                                <label id="signupemail"></label>
                                <input type="text" name="sEmail" id="sEmail" class="form-control">
                            </div>
                            <div class="form-group col-xs-6">
                                <label id="signuppassword"></label>
                                <input type="password" name="sPass" id="sPass" class="form-control">
                            </div>
                            <div class="form-group col-xs-6">
                                <label id="signupconfirm"></label>
                                <input type="password" name="password_confirm" id="password_confirm" class="form-control">
                            </div>
                            <div class="form-group col-xs-12">
                                <br clear="all">
                                <select name="countrySelect" id="countrySelect" class="form-control" onchange="countrySelectChange();">
                                    <option value="0">Select your country / Seleccione su país ...</option>
                                    <option value="CL">Chile</option>
                                    <option value="PE">Peru</option>
                                    <option value="US">United States</option>
                                    <option value="0">--------------------------------</option>
                                    <option value="AR">Argentina</option>
                                    <option value="BZ">Belize</option>
                                    <option value="BO">Boliva</option>
                                    <option value="BR">Brazil</option>
                                    <option value="CO">Colombia</option>
                                    <option value="CR">Costa  Rica</option>
                                    <option value="CU">Cuba</option>
                                    <option value="DO">Dominican Republic</option>
                                    <option value="EC">Ecuador</option>
                                    <option value="SV">El Salvador</option>
                                    <option value="GT">Guatemala</option>
                                    <option value="HN">Honduras</option>
                                    <option value="MX">Mexico</option>
                                    <option value="NI">Nicaragua</option>
                                    <option value="PA">Panama</option>
                                    <option value="PY">Paraguay</option>
                                    <option value="PR">Puerto Rico</option>
                                    <option value="UY">Uruguay</option>
                                    <option value="VE">Venezuela</option>
                                    <option value="0">--------------------------------</option>
                                    <option value="ES">Spain</option>
                                    <option value="CA">Canada</option>
                                    <option value="HT">Haiti</option>
                                    <option value="XX">Other / Otro</option>
                                </select>
                                <input type="text" class="form-control" id="other_country" style="display:none;margin-top:10px;" placeholder="">
                            </div>
                            <div class="form-group col-xs-12 terms">
                                <input type="checkbox" name="course_park_tc" id="course_park_tc" class="termsCheckBox">
                                <a href="" target="_blank" id="signupterms"></a>
                            </div>

                            <div class="form-group col-xs-3 pull-right" style="text-align:right;">
                                <button name="bSignUp_en" id="signupbuttonregister" type="button" class="btn btn-success" onclick="validateForm('reg');"></button>
                            </div>
                        </div>
                        <div id="loginform">
                            <form id="loginForm" name="loginForm" method="post" action="">
                                <div class="form-group col-xs-12">
                                    <label id="signinemail"></label>
                                    <input name="email" type="email" class="form-control" id="email">
                                </div>
                                <div class="form-group col-xs-12">
                                    <label id="signinpassword"></label>
                                    <input name="password" type="password" class="form-control" id="password">
                                    <a href="" id="forgotpassword"></a>
                                </div>
                                <div class="form-group col-xs-12" style="text-align:right;">
                                    <button name="bSignUp_en" id="signinbuttonlogin" type="button" class="btn btn-success" data-toggle="modal" data-target="#progress" data-backdrop="static" onclick="validateForm('login');"></button>
                                </div>
                                <div class="form-group col-xs-12">
                                </div>
                            </form>
                        </div>
                    </div>
                    <!---------------------------------------------------------------------->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="sponsor-logos col-xs-12 col-sm-12 col-md-10 col-lg-10  col-md-offset-1 col-lg-offset-1">
                <div class="col-md-<?php echo(12/$footerlen) ?>">
                    <div id="sponsor1-text" class="sponsor-text "></div>
                    <? echo "<img src='../db-unified/images/logos/sponsors/" . $footers[0] . "'>"; ?>
                </div>
                <div class="col-md-<?php echo(12/$footerlen) ?>">
                    <div id="sponsor2-text" class="sponsor-text "></div>
                    <? echo "<img src='../db-unified/images/logos/sponsors/" . $footers[1] . "'>"; ?>
                </div>

                <?php
                    if ($footerlen == 3) {
                    echo '<div class="col-md-'. (12/$footerlen).'"><div id="sponsor3-text" class="sponsor-text"></div><img src="../db-unified/images/logos/sponsors/' . $footers[2] . '"></div>';
                    }
                ?>

            </div>
        </div>
    </div>
    <script>
    //alert("Count: "+country_list.length);
    $("#other_country").autocomplete({
        lookup: isoCountries,
        onSelect: function (suggestion) {
            //c = $("#other_country").val();
            //console.log(isoCountries[c]);
            country = suggestion.data;
            //console.log('You selected: ' + $("#countrySelect").val());
        }
        
        
        });
    </script>
</body>
</html>