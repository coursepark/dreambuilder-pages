<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>DreamBuilder - Bluedrop Learning Networks</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=9">
  <link href="../db-unified/css/layout.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="../db-unified/css/jqueryui.css" type="text/css">
  <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Raleway">
  <script src="../db-unified/js/jquery-1.4.3.min.js" type="text/javascript"></script>
  <script src="../db-unified/js/system.js" type="text/javascript"></script>
  <script src="../db-unified/js/jqueryui.js" type="text/javascript"></script>
<script>
// INIT values -----------------------------------
var lang = "en";
var mailEnable = 0; // Enable auto mail functions
var emailFrom = "info@coursepark.com"; // Assign FROM address to auto Email

var country = "US";
var live = 1; 
var key = "";
var sec = "";
var activate = 1;
var emailOnly = 0;
var source;
var network = <? echo $network ?>; // Learning Network ID

//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------




 
  </script>
  </head>
  <body>
  <div id="wrapper">
    <div id="bodyLandingPage">
   	  <header id="header">
		<div class="logo" style="margin-left:0px;margin-bottom:0px;">
		  <div align="center"><a><img src="http://dreambuilder.org/wp-content/uploads/2013/05/logo.png" alt="Dreambuilder"></a></div>
		</div>
        <div class="top_bar">
            <div align="center">
              
            </div>
        </div>
        <div id="small-nav">
            <div class="avada-row">
                                <nav id="nav" class="nav-holder">
                                    <div align="center">
                                      
                                    </div>
              </nav>
            </div>
        </div>
	</header>

<div id="formArea">
      <div id="form-logo" >
              <div class="form-logo-align" style="margin-top:125px;margin-left:20px;"><img src="<? echo $logo ?>"></div>
      </div>
      <div align="center">
        <!--<div id="signInTitle"> <span class="txtStyleBlue">¿Está regresando? </span> <span class="txtStylePurple">Iniciar sesi&oacute;n</span></div> -->
      </div>
    <!-- ---------------------------------------------------------------------------------------------------->
      <div id="languageSelect" >
          <p class="langSelectText">Please select one of the following. <br> Por favor seleccione uno de los siguientes.</p>
          <button class="langSelectButton" onclick="flipOver('en');">Enroll (English)</button>
          <button class="langSelectButton" onclick="flipOver('es');">Registrarse (Español)</button>
      </div>
    <!-- ---------------------------------------------------------------------------------------------------->
      <div class="signUpForm" id="signUp_en" >
      <div class="txtFieldAreaSwitch">
                   <h3 align="center"><a style="color:#fff;"  href="javascript:flipToLogin('en');"><img src="../db-unified/images/login.png"></a></h3>
      </div>
         <p align="center">&nbsp;</p>
        
       <h2 align="center"><span class="whiteText">Not a Member? Register Now!</span></h2>
        <form id="form2" class="form2" name="form2" method="post" action="">
          <div class="txtFieldArea1">
            <p align="center">First Name</p>
            <p align="center">
              <input type="text" name="fName" id="fName" class="joinNowtxtFld1">
            </p>
          </div>
          <div class="txtFieldArea1">
            <p align="center">Last Name</p>
            <p align="center">
              <input type="text" name="lName" id="lName" class="joinNowtxtFld1">
            </p>
          </div>
          <div class="txtFieldArea2">
            <p align="center">Email Address</p>
            <p align="center">
              <input type="text" name="sEmail" id="sEmail" class="joinNowtxtFld2">
            </p>
          </div>
          <div class="txtFieldArea1">
            <p align="center">Password</p>
            <p align="center">
              <input type="password" name="sPass" id="sPass" class="joinNowtxtFld1">
            </p>
          </div>
          <div class="txtFieldArea1">
            <p align="center">Confirm Password</p>
            <p align="center">
              <input type="password" name="password_confirm" id="password_confirm" class="joinNowtxtFld1">
            </p>
          </div>
          <div class="termsBox" style="height:25px !important;">
         
              <input type="checkbox" name="course_park_tc" id="course_park_tc" class="termsCheckBox">
         
            <div class="txtStyleFormatTerms" style="color:#00feff;">
              <div><a style="color:#00feff"  href="http://www.coursepark.com/blog/terms-of-service/" target="_blank">I have read the Terms &amp; Conditions</a>
              </div>
            </div>          
            </div>
            <div class="termsBox" style="height:25px !important;" >
         
              <input type="checkbox" name="receiveAnyEmail" id="receiveAnyEmail" class="termsCheckBox">
         
            <div class="txtStyleFormatTerms" style="color:#00feff;">
              <div><a style="color:#00feff" > I agree to receive CoursePark emails containing news, updates and promotions regarding CoursePark products. Please note that you can withdraw your consent at any time.</a>
              </div>
            </div>          
            </div>
  <div class="txtFieldArea1" style="width:275px;height:auto;text-align:center;">
    <div >
      <input name="bSignUp_en" id="bSignUp_en" type="button" class="bSignUp" value="Register">
    </div>
  </div>    
        </form>

    
      </div>
    <!-- ---------------------------------------------------------------------------------------------------->
    <div class="signUpForm" id="signUp_es" style="display: none;">
      <div class="txtFieldAreaSwitch">
                   <h3 align="center"><a style="color:#fff;"  href="javascript:flipToLogin('es');"><img src="../db-unified/images/login_sp.png"></a></h3>
      </div>
         <p align="center">&nbsp;</p>
        
       <h2 align="center"><span class="whiteText">¿No es un miembro? Regístrese ahora</span></h2>
        <form id="form2es" class="form2" name="form2es" method="post" action="">
          <div class="txtFieldArea1">
            <p align="center">Su nombre</p>
            <p align="center">
              <input type="text" name="fName-es" id="fName-es" class="joinNowtxtFld1">
            </p>
          </div>
          <div class="txtFieldArea1">
            <p align="center">Su apellido</p>
            <p align="center">
              <input type="text" name="lName-es" id="lName-es" class="joinNowtxtFld1">
            </p>
          </div>
          <div class="txtFieldArea2">
            <p align="center">Su correo electrónico</p>
            <p align="center">
              <input type="text" name="sEmail-es" id="sEmail-es" class="joinNowtxtFld2">
            </p>
          </div>
          <div class="txtFieldArea1">
            <p align="center">Crear una contraseña</p>
            <p align="center">
              <input type="password" name="sPass-es" id="sPass-es" class="joinNowtxtFld1">
            </p>
          </div>
          <div class="txtFieldArea1">
            <p align="center">confirmar contraseña</p>
            <p align="center">
              <input type="password" name="password_confirm-es" id="password_confirm-es" class="joinNowtxtFld1">
            </p>
          </div>
          <div class="termsBox" style="height:25px !important;">
         
              <input type="checkbox" name="course_park_tc-es" id="course_park_tc-es" class="termsCheckBox">
         
            <div class="txtStyleFormatTerms" style="color:#00feff;">
              <div><a style="color:#00feff"  href="http://www.coursepark.com/blog/terms-of-service/?lang=es" target="_blank">He leído los Términos y Condiciones</a>
              </div>
            </div>          
            </div>
             <div class="termsBox" style="height:25px !important;" >
         
              <input type="checkbox" name="receiveAnyEmail-es" id="receiveAnyEmail-es" class="termsCheckBox">
         
            <div class="txtStyleFormatTerms" style="color:#00feff;">
              <div><a style="color:#00feff" >Estoy de acuerdo en recibir correos electrónicos que contienen noticias, actualizaciones y promociones relativas a producto de CoursePark. Por favor, tenga en cuenta que puede retirar su consentimiento en cualquier momento.</a>
              </div>
            </div>          
            </div>
  <div class="txtFieldArea1" style="width:275px;height:auto;text-align:center;">
    <div >
      <input name="bSignUp-es" id="bSignUp_es" type="button" class="bSignUp" value="Registrar">
    </div>
  </div>
               
        </form>

    
      </div>
    <!-- ---------------------------------------------------------------------------------------------------->
      <div class="signInForm" id="signIn_en" style="display: none;">
             
                   <div class="txtFieldAreaSwitchformtwo">
                     <div align="center"><a style="color:#fff" href="javascript:flipToReg('en');"><img src="../db-unified/images/register.png"></a></div>
                    </div>
         <p align="center">&nbsp;</p>
         <p align="center">&nbsp;</p>
         <p align="center">&nbsp;</p>
         <p align="center">&nbsp;</p>
        <h2 align="center"><span class="whiteText">Already a Member?</span></h2>
        <form id="form1" class="form1" name="form1" method="post" action="https://www.coursepark.com/user/auth/login/url_code?redirect_url=/mycoursepark/?lang=en">
          <p align="center">Your Email</p>
          <p align="center">
            <label for="textfield"></label>
            <input type="text" name="email" id="email" class="textfield">
          </p>
          <p align="center">&nbsp;</p>
          <p align="center">Your Password</p>
          <p align="center">
            <input type="password" name="password" id="password" class="textfield">
          </p>
          <p align="center"><strong><span class="txtStylePassword"><a href="https://www.coursepark.com/user/auth/forgotpassword" target="_blank">Forgot your password?</a></span></strong></p>
          <div class="txtFieldArea1" style="margin-top:10px;width:200px;height:auto;text-align:center;"> 

              <input name="bSignIn" type="button" id="bSignIn_en" class="bSignIn" value="Login">
           
          </div>
          <p align="center">&nbsp;</p>
          <p align="center">&nbsp;</p>
        </form>
      </div> 
 <!-- ---------------------------------------------------------------------------------------------------->
    <div class="signInForm" id="signIn_es" style="display: none;">
             
                   <div class="txtFieldAreaSwitchformtwo">
                     <div align="center"><a style="color:#fff" href="javascript:flipToReg('es');"><img src="../db-unified/images/register_sp.png"></a></div>
                    </div>
         <p align="center">&nbsp;</p>
         <p align="center">&nbsp;</p>
         <p align="center">&nbsp;</p>
         <p align="center">&nbsp;</p>
        <h2 align="center"><span class="whiteText">¿Ya eres miembro?</span></h2>
        <form id="form1es" class="form1" name="form1es" method="post" action="https://www.coursepark.com/user/auth/login/url_code?redirect_url=/mycoursepark/?lang=en">
          <p align="center">Su correo electrónico</p>
          <p align="center">
            <label for="textfield"></label>
            <input type="text" name="email-es" id="email-es" class="textfield">
          </p>
          <p align="center">&nbsp;</p>
          <p align="center">Su contraseña</p>
          <p align="center">
            <input type="password" name="password-es" id="password-es" class="textfield">
          </p>
          <p align="center"><strong><span class="txtStylePassword"><a href="https://www.coursepark.com/user/auth/forgotpassword" target="_blank">Olvidé mí contraseña</a></span></strong></p>
          <div class="txtFieldArea1" style="margin-top:10px;width:200px;height:auto;text-align:center;"> 

              <input name="bSignIn_es" type="button" id="bSignIn_es" class="bSignIn bSignInsp" value="Iniciar sesión">
            
           
          </div>
          <p align="center">&nbsp;</p>
          <p align="center">&nbsp;</p>
        </form>
      </div>
     <!-- ---------------------------------------------------------------------------------------------------->

   </div> <!--end wrapper-->
  </div><!--end modylanding-->
  <div id="footer">

    <div   id="copyright"><img src="<? echo $footer ?>" title="Logo"></div>
    
    
</div>
      
  <script>
  
  </script>





</body>
</html>