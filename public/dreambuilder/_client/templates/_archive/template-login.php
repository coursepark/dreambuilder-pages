<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>DreamBuilder - Bluedrop Learning Networks</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=10">
  
  <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Raleway">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <link href="../db-unified/css/db-unified.css" rel="stylesheet" type="text/css">
    
  <script src="https://code.jquery.com/jquery-2.1.3.min.js" type="text/javascript"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <script src="../db-unified/js/functions.js" type="text/javascript"></script>
  
<script>
// INIT values -----------------------------------
var defaultLang = "<?php
echo $default_lang ?>"; // Default language for page
var include_langs = "<?php
echo $include_langs ?>";  // languages for page
var current = "<?php
echo $current ?>"
var network = <?php
echo $network ?>;
var footerlen = <?php
echo $footerlen ?>;
var logo = "<?php echo $logo ?>";
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------




 
</script>
</head>
<body>
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
   
      
      <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 pageheader col-md-offset-1 col-lg-offset-1">
      <a href="#"><img src="http://dreambuilder.org/wp-content/uploads/2013/05/logo.png"></a>
          </div></div>
   

  
      <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10  col-md-offset-1 col-lg-offset-1">
              <div class="form-logo-block row">
                  <div class="logo-block col-md-4">
                  <?php
                 if ($extlink) {
                      echo '<a href="'.$extlink.'" target=_blank><img src="'.$logo.'"></a>';
                    } else {
                     echo '<img src="'. $logo .'">';
                  }
                  ?>
</div>
                  <div class="form-block col-xs-12 col-sm-12 col-md-8 col-lg-8">
                     <div class="col-xs-12 regswitch"><ul class="nav nav-tabs">
                          <li class="formtitle" id="formtitle"></li>
  <li id ="tab_reg" role="presentation" style="display:none" class="active"><a href="javascript:switchTab('reg');"><span id="navregister"></span></a></li>
  <li id="tab_login" role="presentation" ><a href="javascript:switchTab('login');"><span id="navlogin"></span></a></li>
  
</ul></div>
                  <!--------------------------------------------------------------------->
                      <!--<h2 align="center"><span class="whiteText">Not a Member? Register Now!</span></h2>-->
        <div id="regloginform" class=" col-xs-12 formborder" >
            <div class="form-group col-xs-12">
                <div id="languageSelectDropdown"></div>
                
                
                </div>
          <div id="regform">
                    <div class="form-group col-xs-6">
                <label id="signupfirstname"></label>
                  <input type="text" name="fName" id="fName" class="form-control">
              </div>
                    <div class="form-group col-xs-6">
                <label id="signuplastname"></label>
                  <input type="text" name="lName" id="lName" class="form-control">
              </div>
              
              <div class="form-group col-xs-12">
                <label id="signupemail"></label>
                  <input type="text" name="sEmail" id="sEmail" class="form-control">
              </div>
              
                
              <div class="form-group col-xs-6">
                <label id="signuppassword"></label>
                  <input type="password" name="sPass" id="sPass" class="form-control">
              </div>
                
                
              <div class="form-group col-xs-6">
                <label id="signupconfirm"></label>
                  <input type="password" name="password_confirm" id="password_confirm" class="form-control">
              </div>
                
                
              <div class="form-group col-xs-12 terms">
             
                  <input type="checkbox" name="course_park_tc" id="course_park_tc" class="termsCheckBox">
             <a href="http://www.coursepark.com/blog/terms-of-service/" target="_blank" id="signupterms"></a>
                </div>
                
                
               <!-- <div class="form-group col-xs-9 terms" >
                  <input type="checkbox" name="receiveAnyEmail" id="receiveAnyEmail" class="termsCheckBox">
             <span id="signupagree"></span>
                </div>-->
                
            <div class="form-group col-xs-3 pull-right" style="text-align:right;">
        
               <button name="bSignUp_en" id="signupbuttonregister" type="button" class="btn btn-success" onclick="validateForm('reg');"></button>
        
            </div>

      </div>
            <div id="loginform">
            <form id="loginForm" name="loginForm" method="post" action="">
            <div class="form-group col-xs-12">
                <label id="signinemail"></label>
                <input name="email" type="email" class="form-control" id="email">
                                                             
                </div>
            <div class="form-group col-xs-12">
                <label id="signinpassword"></label>
                <input name="password" type="password" class="form-control" id="password"> 
                <a href="" id="forgotpassword"></a>
                </div>
             <div class="form-group col-xs-12" style="text-align:right;">
                 <button name="bSignUp_en" id="signinbuttonlogin" type="button" class="btn btn-success" data-toggle="modal" data-target="#progress" data-backdrop="static" onclick="validateForm('login');"></button>

                </div>
            <div class="form-group col-xs-12">
                
                </div>
                </form>
            </div>
        </div>
                      <!---------------------------------------------------------------------->
                  </div>
                  </div>
             
              </div>
          
      </div>
       
    <div class="row">
    <div class="sponsor-logos col-xs-12 col-sm-12 col-md-10 col-lg-10  col-md-offset-1 col-lg-offset-1">
      <div class="col-md-<?php echo(12/$footerlen) ?>">
        <div id="sponsor1-text" class="sponsor-text "></div>
        <? echo "<img src='../db-unified/images/logos/sponsors/" . $footers[0] . "'>"; ?> 
      </div>
      <div class="col-md-<?php echo(12/$footerlen) ?>">
        <div id="sponsor2-text" class="sponsor-text "></div>
        <? echo "<img src='../db-unified/images/logos/sponsors/" . $footers[1] . "'>"; ?> 
      </div>
      
    <?php
    if ($footerlen == 3) {
      echo '<div class="col-md-'. (12/$footerlen).'"><div id="sponsor3-text" class="sponsor-text"></div><img src="../db-unified/images/logos/sponsors/' . $footers[2] . '"></div>';
    }
?>
       
    </div>
    </div>
</div>

<!-------------------------------------------------------------------------------------------->
<script>
    switchTab('login');
    </script>
  </body>
  </html>