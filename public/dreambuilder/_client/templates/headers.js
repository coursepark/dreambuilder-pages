 //JS
 var headers = '<script src="https://code.jquery.com/jquery-2.1.3.min.js" type="text/javascript"></script>' +
     '<script src="/_client/js/language_strings.js" type="text/javascript"></script>' +
     '<script src="/_client/js/country_codes.js" type="text/javascript"></script>' +
     //'<script src="/_client/js/clientsettings.js" type="text/javascript"></script>' +
     '<script src="/_client/js/functions.js" type="text/javascript"></script>' +
     '<title>DreamBuilder - Bluedrop Learning Networks</title>' +
     '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' +
     '<meta name="viewport" content="width=device-width, initial-scale=1">' +
     '<meta http-equiv="X-UA-Compatible" content="IE=10">' +
     '<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">' +
     '<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway">' +
     '<link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet" type="text/css">' +
     '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">' +
     '<link href="/_client/css/db-unified.css" rel="stylesheet" type="text/css">' +
     '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>' +
     '<script src="https://use.fontawesome.com/96451dd8eb.js"></script>'+
	 '<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.devbridge-autocomplete/1.2.25/jquery.autocomplete.min.js"></script>';
 document.write(headers);