//  ------------------------------------------------------------------------------------------
var lang;
var activetab = "";
var CLForm;
var maraform;
var include_langs;
var network;
var footerlen;
var userID;
var customSettings;
var clientName;
var em, fn, ln, pa, pass, userid, learningnetworkId, pu, pro, country, ch, panew, panewc;
var urlParams = {};
var siteURL, coreURL, apiURL;
var metaData = {};
var parent = "dreambuilder";
metaData["address"] = {};

//  ------------------------------------------------------------------------------------------
var loadingModal = '<div class="modal fade" id="loadingControl" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><div id="spinner"><img src="/_client/images/loading-big.gif"></div><div class="loadingdisplay" id="loadingDisplay">Loading...</div></div></div></div></div>';
//  ------------------------------------------------------------------------------------------
var postCardModal = '<div class="modal fade" id="postcard" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog" style="width:720px"><div class="closebutton pull-right" onclick="javascript:toggleModalWindow(\'postcard\',\'hide\');"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-times fa-stack-1x fa-inverse"></i></span></div><div class="postcard"><div id="postcardtext"></div><div class="alma"><img src="/_client/images/alma.png"></div><div class="address"><a class="btn btn-primary lmore" href="" id="videolink" target="_blank"><span class="fa fa-video-camera"></span> <span id="postcardvideo"></span></a><hr /><input type="password" id="newPass" placeholder="" class="form-control"><input type="password" id="newPassConfirm" placeholder="" class="form-control"><button class="btn btn-success acceptterms" onclick="postcardAccept();"></button></div></div></div></div>';
//  ------------------------------------------------------------------------------------------
var postCardFinanceModal = '<div class="modal fade" id="postcardFinance" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog" style="width:720px"><div class="closebutton pull-right" onclick="javascript:toggleModalWindow(\'postcardFinance\',\'hide\');"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-times fa-stack-1x fa-inverse"></i></span></div><div class="postcard"><div id="postcardFinancetext"></div><button class="btn btn-success acceptFinance" onclick="getStarted();"></button><div class="alma"><img src="/_client/images/alma.png"></div></div></div></div>';
//  ------------------------------------------------------------------------------------------
var termsModal = '<div class="modal fade" id="termsconditions" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-header bg-primary"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title" id="termsTitle"></h4></div><span id="termsHolder"></span><div class="modal-footer"><button style="margin-top:1px;" class="btn btn-success btn-block acceptterms" onclick="acceptTerms();"></button></div></div></div>';
//  ------------------------------------------------------------------------------------------
var videoModal = '<div class="modal fade" id="videopopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-header bg-primary"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title" id="videoTitle"></h4></div><span id="videoHolder"><iframe width="100%" height="315" src="" frameborder="0" allowfullscreen id="iFrameSrc"></iframe></span><div class="modal-footer"></div></div></div>';
//  ------------------------------------------------------------------------------------------
var postCardNotice = '<div class="modal fade" id="postcardNotice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog" style="width:720px"><div class="closebutton pull-right" onclick="javascript:toggleModalWindow(\'postcardNotice\',\'hide\');"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-times fa-stack-1x fa-inverse"></i></span></div><div class="postcard"><div id="postcardNoticetext"></div><button class="btn btn-success acceptNotice" onclick="toggleModalWindow(\'postcardNotice\', \'hide\');"></button><div class="alma"><img src="/_client/images/alma.png"></div></div></div></div>';
//  ------------------------------------------------------------------------------------------
//
//  ------------------------------------------------------------------------------------------
$(document).ready(function () {
	//--------------------------------------------------------------------
	if (location.protocol == 'http:' && location.hostname != 'localhost') {
		location.href = location.href.replace(/^http:/, 'https:')
	}

	(window.onpopstate = function () {
		var match,
			pl = /\+/g, // Regex for replacing addition symbol with a space
			search = /([^&=]+)=?([^&]*)/g,
			decode = function (s) {
				return decodeURIComponent(s.replace(pl, " "));
			},
			query = window.location.search.substring(1);


		while (match = search.exec(query))
			urlParams[decode(match[1])] = decode(match[2]);
	})();
	clientName = urlParams.cl.trim();
	
	//--------------------------------------------------------------------
	if (urlParams.cty && clientName == "esp") {
		switch (urlParams.cty) {
			case "US":
			case "us": 
				clientName = "mvsus";
				break;
			case "PE":
			case "pe":
				clientName = "mvspe";
				break;
			case "CL":
			case "cl":
				clientName = "mvscl";
				break;
			default:
				clientName = "esp";
		}
	}
	//--------------------------------------------------------------------
	if (clientName) {
		if (clientName.indexOf("/") > 0) {
			clientName = clientName.substr(0, clientName.indexOf("/"));
		}

		clientName = clientName.toLowerCase();
		injectModal(loadingModal);
		siteURL = 'https://' + location.hostname;

		if (siteURL === "https://localhost") {
			siteURL = 'https://dreambuilder.qc.bluedrop.io';
		}

		$.when(dataLoader('env', '', siteURL).done(function (data) {
			coreURL = "https://" + parent + data.coreURL;
			apiURL = data.apiURL;
			clientLoad();
		}));
	}
});

function init() {
	$("body").load("/_client/templates/pages/" + clientSettings.template, function (responseTxt, statusTxt, xhr) {
		include_langs = clientSettings.include_langs;
		network = clientSettings.network;
		logo = clientSettings.logo;
		memfilters = (clientSettings.member_filters) ? memberFilters[clientSettings.member_filters] : clientSettings.member_filters;
		if (clientSettings.logo) {
			$("#client_logo").html("<a id='logoLink'><img src='" + clientSettings.logo + "'></a>");
			if (clientSettings.extLink) {
				$("#logoLink").attr('href', clientSettings.extLink);
				$("#logoLink").attr('target', '_blank');
			}
		}
		if (clientSettings.footers) {
			footerlen = clientSettings.footers.length;
			buildFooters();
		}
		if (clientSettings.styles) {
			$('head').append('<link rel="stylesheet" href="/_client/css/' + clientSettings.styles + '" type="text/css" />');
		}
		if (clientSettings.services.length == 1) {
			$("#tab_reg").css("display", "none");
			$("#tab_login").css("display", "none");

			$("#regformblock").css("display", "none");
			$("#loginformblock").css("display", "none");

			$("#" + clientSettings.services[0] + "formblock").css("display", "block");

			activetab = clientSettings.services[0];
			switchTab(clientSettings.services[0]);
		}
		////////////////////////////////////////////////////////////////////////////////////////////////

		if (localStorage.getItem("lastemail")) {
			$("#email").val(localStorage.getItem("lastemail"))
		}
		$('[data-toggle="tooltip"]').tooltip({
			trigger: "click"
		});
		//---------------------------------------------
		////////////////////////////////////////////////////////////////////////////////////
		getLang();
		injectModal(postCardModal);
		injectModal(postCardFinanceModal);
		injectModal(postCardNotice);
		injectModal(termsModal);
		injectModal(loadingModal);
		//injectModal(videoModal);
		buildLanguageSelection();
		if (clientSettings.member_filters) {
			buildMemberFilters();
			if (clientName == "program") {
				memfilters = false;
				$("#member-filters").css('display', 'none');

			} else {
				$("#member-filters").css("display", "block");
			}

		}
		if (clientSettings.region_filter) {
			if (typeof clientSettings.region_filter == 'string') {
				g = new Array();
				g[0] = clientSettings.region_filter;
				clientSettings.region_filter = g;
			}
			buildRegionSelection(clientSettings.region_filter);
			$("#region-filter").css("display", "block");
		}
		setLanguageStrings();
		
		var activeTab = localStorage.getItem("gologin");
		if (activeTab) {
			switchTab('login');
		} else {
			switchTab('reg');
		}
		
		if (clientSettings.index) {
			customSettings = scrubber(clientSettings.index);
			if (customSettings.defaults) {
				for (var k in customSettings.defaults) {
					clientSettings[k] = customSettings.defaults[k];
				}
			}

			buildOptionDropdown();
		}
		$("body").append('<div id="impNotice"><p><strong>Important Notice</strong></p><p><strong>Dear DreamBuilder student</strong>, We’ve identified a compatibility issue between DreamBuilder and the Internet Explorer and Safari browsers. Please continue your DreamBuilder journey but, we would ask that you use another browser, for example either <a href="https://www.google.com/chrome/" target="_blank">Chrome</a> or <a href="https://www.mozilla.org/en-CA/firefox/new/" target="_blank">Firefox</a>. We are currently working on a fix and hope to have functionality restored soon. Best, DreamBuilder Team</p><p><strong>Estimado estudiante de DreamBuilder</strong>, HHemos identificado un problema de compatibilidad entre DreamBuilder e Internet Explorer y los navegadores Safari. Continúe su viaje a DreamBuilder, pero le pediremos que use otro navegador, por ejemplo, <a href="https://www.google.com/chrome/" target="_blank">Chrome</a> y <a href="https://www.mozilla.org/en-CA/firefox/new/" target="_blank">Firefox</a>. Actualmente estamos trabajando en una solución y esperamos que la funcionalidad se restaure pronto. Mejor, Equipo DreamBuilder</p></div>');
		$("#impNotice").append('<div id="impClose" onclick="closeImpNotice()"><i class="fa fa-times-circle"></i></div>');
	});
	// Inject IE warning message 
	

}
function closeImpNotice() {
	$("#impNotice").remove();
}
//  ------------------------------------------------------------------------------------------
function scrubber(v) {
	if ($.type(v) === "string") {
		return new Function('return ' + v)();
	}
}
//  ------------------------------------------------------------------------------------------
function buildFooters() {
	blockCount = 12 / parseInt(footerlen);
	for (i = 0; i < footerlen; i++) {
		$(".sponsor-logos").append("<div class='col-xs-" + blockCount + "'><div id='slabel" + i + "-text' class='sponsor-text'></div><img src='/_client/images/logos/sponsors/" + clientSettings.footers[i] + "'></div>");
	}
}
//  ------------------------------------------------------------------------------------------
function switchTab(who) {
	if (clientSettings.services.length > 1) {
		$("#regformblock").css("display", "none");
		$("#loginformblock").css("display", "none");
		$("#tab_reg").removeClass("active");
		$("#tab_login").removeClass("active");
		$("#" + who + "formblock").css("display", "block");
		$("#tab_" + who).addClass("active");
		activetab = who;
		$("#formtitle").html(languageStrings[lang][who + "Title"]);
	}
}
//  ------------------------------------------------------------------------------------------
function buildOptionDropdown() {
	obj = customSettings;

	len = Object.keys(obj.options).length;
	if (len > 1) {
		$("#selectDropdown").html('<br><select class="form-control" id="' + clientSettings.index + '" onChange="updateClientSettings()"></select>');
	}
	selectdescrip = '';
	for (x = 0; x < include_langs.length; x++) {
		selectdescrip += obj.languageStrings[include_langs[x]].selecttext;
		if (x < include_langs.length - 1) {
			selectdescrip += " / ";
		}
	}
	$("#" + clientSettings.index).append(new Option(selectdescrip, "0"));
	$("#" + clientSettings.index).append(new Option("--------------------", "0"));
	for (var k in obj.options) {
		if (obj.options.hasOwnProperty(k)) {
			$("#" + clientSettings.index).append(new Option(k, k));
		}
	}
	$.extend(alerts[lang], obj.alerts[lang]);
}
//  ------------------------------------------------------------------------------------------
function updateClientSettings() {
	var val = $("#" + clientSettings.index).val();
	for (var k in customSettings.options[val]) {
		clientSettings[k] = customSettings.options[val][k];
	}
	if (customSettings.callback) {
		var tmpFunc = new Function(customSettings.callback + "()");
		tmpFunc();
	} else {
		networkLookup('noinit');
	}
}
//  ------------------------------------------------------------------------------------------
function buildLanguageSelection() {
	//strings = include_langs.split(",");

	if (include_langs.length > 1) {
		$("#languageSelectDropdown").html('<select class="form-control" onChange="updateLanguage();" id="languageSelect"></select>');
	}
	len = Object.keys(languageStrings).length;
	selectdescrip = "";
	for (x = 0; x < include_langs.length; x++) {
		selectdescrip += languageStrings[include_langs[x]].selecttext;
		if (x < include_langs.length - 1) {
			selectdescrip += " / ";
		}
	}
	$("#languageSelect").append(new Option(selectdescrip, "0"));
	for (x = 0; x < include_langs.length; x++) {
		$("#languageSelect").append(new Option(languageStrings[include_langs[x]].language, include_langs[x]));
	}
}
//  ------------------------------------------------------------------------------------------
function buildRegionSelection(objArray) {
	$("#regionDropdown").html = '';
	
	for (x = 0; x < objArray.length; x++) {
		obj = scrubber(objArray[x]);
		len = Object.keys(obj).length;
		if (len > 1) {
			$("#regionDropdown").append('<br><select class="form-control" id="regionSelect_' + objArray[x] + '"></select>');
		}
		selectdescrip = '';
		for (v = 0; v < include_langs.length; v++) {
			selectdescrip += obj.languageStrings[include_langs[v]].select;
			if (v < include_langs.length - 1) {
				selectdescrip += " / ";
			}
		}
		$("#regionSelect_" + objArray[x]).append(new Option(selectdescrip, "0"));
		$("#regionSelect_" + objArray[x]).append(new Option("--------------------", "0"));
		for (var k in obj.list) {
			if (obj.list.hasOwnProperty(k)) {
				$("#regionSelect_" + objArray[x]).append(new Option(k, obj.list[k]));
			}
		}
		$.extend(alerts[lang], obj.alerts[lang]);
	}
}

//  ------------------------------------------------------------------------------------------
function buildMemberFilters() {
	$("#member-filters").html('');
	hld = '';
	if (memfilters.type == "text") {
		for (i = 0; i < memfilters.options.length; i++) {
			hld += '<div class="form-group col-xs-' + (12 / memfilters.options.length) + '">';
			hld += '<label id="member_' + memfilters.options[i] + '"></label>';
			hld += '<input type="text" name="mem_' + memfilters.options[i] + '" id="mem_' + memfilters.options[i] + '" class="form-control">';
			hld += '</div>';
		}
	}
	if (memfilters.type == "radio") {
		for (i = 0; i < memfilters.options.length; i++) {
			hld += '<div class="form-group col-xs-' + (12 / memfilters.options.length) + '">';
			hld += '<input type="radio" name="' + memfilters.label + '" value="' + memfilters.options[i] + '" id="mem_' + memfilters.options[i] + '"> ';
			hld += '<label id="member_' + memfilters.options[i] + '" for="mem_' + memfilters.options[i] + '"></label>';
			hld += '</div>';
		}
	}
	$("#member-filters").append(hld);

}
//  ------------------------------------------------------------------------------------------
function setLanguageStrings(language) {
	if (!language) {
		getLang();
	} else {
		lang = language;
	}
	if (memfilters) {
		$.extend(languageStrings[lang], memfilters.languageStrings[lang]);
		$.extend(alerts[lang], memfilters.alerts[lang]);
	}
	if (activetab == "login") {
		$("#formtitle").html(languageStrings[lang].loginTitle);
	} else {
		$("#formtitle").html(languageStrings[lang].registerTitle);
	}
	// SET LANGUAGE ITEMS REGISTER FORM ---------------------------------------------
	$("#navregister").html(languageStrings[lang].navRegister);
	$("#navlogin").html(languageStrings[lang].navLogin);
	$("#signupfirstname").html(languageStrings[lang].firstname);
	$("#signuplastname").html(languageStrings[lang].lastname);
	$("#signupemail").html(languageStrings[lang].email);
	$("#signuppassword").html(languageStrings[lang].password);
	$("#signupconfirm").html(languageStrings[lang].confirm);
	$("#signupterms").html(languageStrings[lang].terms);
	$("#countryselecttext").html(languageStrings[lang].countryselecttext);
	$("#signupterms").attr("href", "javascript:toggleModalWindow('termsconditions','show');");
	if (memfilters) {
		for (i = 0; i < memfilters.options.length; i++) {
			$("#member_" + memfilters.options[i]).html(languageStrings[lang]["member_" + memfilters.options[i]]);
		}
	}
	// if (clientSettings[clientName].region_filter) {
	// 	$("#region-label").html(languageStrings[lang].stateselect);
	// }
	$("#signupagree").html(languageStrings[lang].agree);
	$("#signupbuttonregister").html(languageStrings[lang].navRegister);
	$("#postcardtext").html(languageStrings[lang].postcardtext);
	$("#postcardFinancetext").html(languageStrings[lang].postcardFinancetext);
	$("#postcardNoticetext").html(languageStrings[lang].postcardNoticetext);
	$("#newPass").attr("placeholder", languageStrings[lang].password);
	$("#newPassConfirm").attr("placeholder", languageStrings[lang].confirm);
	$("#postcardvideo").html(languageStrings[lang].postcardvideo);
	$(".acceptterms").text(languageStrings[lang].postcardbutton);
	$(".acceptFinance").text(languageStrings[lang].postcardFinancebutton);
	$(".acceptNotice").text(languageStrings[lang].postcardNoticebutton);
	// SET LANGUAGE ITEMS LOGIN FORM ------------------------------------------------
	$("#signinemail").html(languageStrings[lang].email);
	$("#signinpassword").html(languageStrings[lang].passwordlogin);
	$("#forgotpassword").html(languageStrings[lang].forgotpassword);
	$("#forgotpassword").attr("href", coreURL + "/password/forgot?lang=" + lang);
	$("#signinbuttonlogin").html(languageStrings[lang].navLogin);
	// EXTRA NOTICE  -----------------------------------------------------------------------
	$("#extraNotice").html(languageStrings[lang].extraNotice);
	// FOOTERS -----------------------------------------------------------------------
	$("#slabel0-text").html(languageStrings[lang].sponsor0);
	$("#slabel1-text").html(languageStrings[lang].sponsor1);
	if (footerlen == 3) {
		$("#sponsor2-text").html(languageStrings[lang].sponsor2);
	}
	$("#other_country").attr("placeholder", languageStrings[lang].othermessage)
	// TERMS -------------------------------------------------------------------------
	$("#termsHolder").load("/_client/templates/toc/eula-" + lang + ".html", function (responseTxt, statusTxt, xhr) {});
	$("#termsTitle").html(languageStrings[lang].termsTitle);
	$("#videolink").attr('href', 'https://youtu.be/' + languageStrings[lang].youtubeVideo)
	$("#videoTitle").html(languageStrings[lang].postcardvideo);
	
	if (activetab) {
		switchTab(activetab);
	}

}
//  ------------------------------------------------------------------------------------------
function injectModal(modal) {
	$('body').append(modal);
}
//  ------------------------------------------------------------------------------------------
function toggleModalWindow(who, toggle) {
	$("#" + who).modal({});
	$('#' + who).modal(toggle);
}
//  ------------------------------------------------------------------------------------------
function hideModal() {
	$('#loadingControl').modal('hide');
	switchTab("login");
}
//  ------------------------------------------------------------------------------------------
function showVideo() {
}
//  ------------------------------------------------------------------------------------------
function updateLanguage() {
	if ($("#languageSelect").val() != 0) {
		lang = $("#languageSelect").val();
		setLanguageStrings(lang);
	}
	if (clientName == 'program') {
		buildOptionDropdown();
	}
}
//  ------------------------------------------------------------------------------------------
function getLang() {
	if (localStorage.getItem("lang")) {
		if (localStorage.getItem("client") == clientName) {
			lang = localStorage.getItem("lang");
			activetab = "login";
			$("#languageSelect").val(lang);
			return lang;
		} else {
			lang = include_langs[0];
		}
	} else {
		lang = include_langs[0];

	}
	if (urlParams.lang != null) {
		lang = urlParams.lang;
		activetab = "login";
		$("#languageSelect").val(lang);
		return lang;
	} else {
		lang = include_langs[0];

	}
}
//  ------------------------------------------------------------------------------------------
function validateEmail(elementValue) {
	var emailPattern = /^[a-zA-Z0-9.\_\-\+]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	return emailPattern.test(elementValue);
}
//  ------------------------------------------------------------------------------------------
function validatePassword(elementValue) {
	var passwordPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
	return passwordPattern.test(elementValue);
}

//  ------------------------------------------------------------------------------------------
function validateForm(which) {
	switch (which) {
		case "reg":
			if ($("#languageSelect").val() == "0") {
				showError("languageSelect", "langselect");
				return false;
			}
			if (!$("#fName").val() || !$("#lName").val()) {
				showError("fName", "bothnames");
				return false;
			}
			if (!validateEmail($("#sEmail").val())) {
				showError("sEmail", "email");
				return false;
			}
			if (!validatePassword($("#sPass").val())) {
				showError("sPass", "tooshort");
				return false;
			}
			if ($("#sPass").val() != $("#password_confirm").val()) {
				showError("password_confirm", "passfail");
				return false;
			}
			if (CLForm == 1) {
				if ($("#countrySelect").val() == "0") {
					showError("countrySelect", "country");
					return false;
				}
			}
			if (maraform == 1) {
				if (!$("#countrySelect").val()) {
					showError("countrySelect", "country");
					return false;
				}
			}
			if (memfilters != false && memfilters.type == 'text') {
				for (i = 0; i < memfilters.options.length; i++) {
					if (!$("#mem_" + memfilters.options[i]).val()) {
						showError("mem_" + memfilters.options[i], memfilters.options[i]);
						return false;
						break;
					}
				}
			}
			if (memfilters != false && memfilters.type == 'radio') {
				if (!$('input[name=' + memfilters.label + ']').is(':checked')) {
					showError("mem_" + memfilters.options[0], memfilters.label);
					return false;
				}
			}
			if (clientSettings.region_filter.length > 0) {

				for (i = 0; i < clientSettings.region_filter.length; i++) {
					if ($("#regionSelect_" + clientSettings.region_filter[i]).val() == "0") {
						showError("regionSelect_" + clientSettings.region_filter[i], clientSettings.region_filter[i]);
						return false;
					}
				}
			}
			
			if (clientSettings.index) {
				if ($("#" + clientSettings.index).val() == "0") {
					showError(clientSettings.index, clientSettings.index);
					return false;
				}
			}
			
			if (!$("#course_park_tc").is(':checked')) {
				showError("course_park_tc", "terms");
				return false;
			}
			break;
		case "login":
			if ($("#languageSelect").val() == "0") {
				showError("languageSelect", "langselect");
				return false;
			}
			if (!validateEmail($("#email").val())) {
				showError("email", "email");
				return false;
			}
			if ($("#password").val().length < 6) {
				showError("password", "tooshort");
				return false;
			}
			break;
	}
	getVars(which);
}
//  ------------------------------------------------------------------------------------------
function showError(who, which) {
	$('#' + who).tooltip({
		title: alerts[lang]["alert-" + which],
		placement: "left",
		trigger: "click"
	});
	$('#' + who).tooltip('show');
	$('#' + who).on('hidden.bs.tooltip', function () {
		$('#' + who).tooltip('destroy');
	});
}
//  ------------------------------------------------------------------------------------------
function updateLoadingText(what, confirm, lo) {
	if (lo) {
		$("#loadingDisplay").css("display", "none");
	} else {
		$("#loadingDisplay").css("display", "block");
		$("#loadingDisplay").html(what);
	}
	$("#loadingControl").modal({
		backdrop: "static"
	});
	$('#loadingControl').modal('show');

	if (confirm) {
		$("#loadingDisplay").append("<br><br><button class='btn btn-warning btn-lg' onclick='hideModal()'>Ok</button>");
		$("#spinner").css("display", "none");
	}
}
//  ------------------------------------------------------------------------------------------
function countrySelectChange() {
	country = clientSettings.country;
	if (clientName == "program" || clientName == "esp") {
		if (country != "PE" && country != "CL") {
			$("#member-filters").css('display', 'none');
			memfilters = false;
		} else {
			memfilters = (clientSettings.member_filters) ? memberFilters[clientSettings.member_filters] : clientSettings.member_filters;
			setLanguageStrings(lang);
			$("#member-filters").css('display', 'block');
		}
		if (country == "US") {
			clientSettings.region_filter = ['states'];
			buildRegionSelection(clientSettings.region_filter);
			$("#region-filter").css("display", "block");
		} else {
			clientSettings.region_filter = false;
			$("#region-filter").css("display", "none");
		}
	}
	if (country == "XX") {
		$("#other_country").css("display", "block");
	} else {
		$("#other_country").css("display", "none");
		if (clientName == "program") {
			networkSelect();
		}
	}
	$("#other_country").autocomplete({
		lookup: isoCountries,
		onSelect: function (suggestion) {
			clientSettings.country = suggestion.data;
			if (clientName == "program") {
				networkSelect();
			}
		}
	});
}
//  ------------------------------------------------------------------------------------------
function mvsSelectChange() {
	country = clientSettings.country;
	if (clientName == "esp") {
		switch (country) {
			case "US":
				clientSettings.region_filter = ['states'];
				buildRegionSelection(clientSettings.region_filter);
				$("#region-filter").css("display", "block");
				break;
			default:
				clientSettings.region_filter = false;
				$("#region-filter").css("display", "none");
				break;
		}
		updateProg();
	}
	if (country == "XX") {
		$("#other_country").css("display", "block");
	} else {
		$("#other_country").css("display", "none");
	}
	$("#other_country").autocomplete({
		lookup: isoCountries,
		onSelect: function (suggestion) {
			clientSettings.country = suggestion.data;
			updateProg()
		}
	});
}
//  ------------------------------------------------------------------------------------------
function amSpacesSelectChange() {
	country = clientSettings.country;
	updateProg();
	if (country == "XX") {
		$("#other_country").css("display", "block");
	} else {
		$("#other_country").css("display", "none");
		updateProg();
	}
	$("#other_country").autocomplete({
		lookup: isoCountries,
		onSelect: function (suggestion) {
			clientSettings.country = suggestion.data;
			updateProg();
		}
	});
}
//  ------------------------------------------------------------------------------------------
function aweSelectChange() {
	country = clientSettings.country;
	if (country == "XX") {
		$("#other_country").css("display", "block");
	} else {
		$("#other_country").css("display", "none");
		// if (clientName == "program") {
		// 	networkSelect();
		// }
	}
	$("#other_country").autocomplete({
		lookup: isoCountries,
		onSelect: function (suggestion) {
			clientSettings.country = suggestion.data;
			
			// if (clientName == "program") {
			// 	networkSelect();
			// }
		}
	});
}
//  ------------------------------------------------------------------------------------------
function utahSelectChange() {
	networkLookup('noinit');
}
//  ------------------------------------------------------------------------------------------
function updateProg() {
	switch (lang) {
		case "en":
			clientSettings["en-program_id"] = "enProgram";
			break;
		case "es":
			switch (clientSettings.country) {
				case "CL":
					clientSettings["es-program_id"] = "clProgram";
					break;
				case "PE":
					clientSettings["es-program_id"] = "peProgram";
					break;
				case "US":
					clientSettings["es-program_id"] = "peProgram";
					break;
				default:
					clientSettings["es-program_id"] = "peProgram";
					break;
			}
			break;
	}
}
//  ------------------------------------------------------------------------------------------
function getVars(which) {
	pitem = lang + "-program_id";
	pro = clientSettings[pitem];
	network = clientSettings.network;
	switch (which) {
		case "reg":
			fn = $("#fName").val();
			ln = $("#lName").val();
			em = $("#sEmail").val();
			pa = $("#sPass").val();
			ch = clientSettings.channel_id;
			pu = clientSettings.private_profile;
			if (memfilters != false) {
				if (memfilters.type == 'text') {
					for (i = 0; i < memfilters.options.length; i++) {
						metaData[memfilters.options[i]] = encodeURIComponent($("#mem_" + memfilters.options[i]).val());
					}
				}
				if (memfilters.type == 'radio') {
					metaData[memfilters.label] = $('input[name=' + memfilters.label + ']:checked').val();
				}
			}
			if (clientSettings.region_filter) {
				for (i = 0; i < clientSettings.region_filter.length; i++) {
					var s = scrubber(clientSettings.region_filter[i]).storage;
					if (typeof s == 'string') {
						metaData[s] = $("#regionSelect_" + clientSettings.region_filter[i]).val();
					} else {
						var holdA = {};
						holdA[s[1]] = $("#regionSelect_" + clientSettings.region_filter[i]).val();
						var holdB = {};
						holdB[s[0]] = holdA;;
						$.extend(metaData, holdB);
					}
				}
			}
			
			if (clientSettings.index) {
				if (scrubber(clientSettings.index).storage) {
					metaData["address"][scrubber(clientSettings.index).storage] = $("#" + clientSettings.index).val();
				}
			}
			metaData["address"]["country-name"] = clientSettings.country;
			createAcct();
			break;
		case "login":
			em = $("#email").val();
			pa = $("#password").val();
			goLogin();
			break;
	}
}
//  ------------------------------------------------------------------------------------------
function acceptTerms() {
	$('#postcard').modal('hide');
	$('#termsconditions').modal('hide');
	$("#course_park_tc").prop('checked', 'true')
}
//  ------------------------------------------------------------------------------------------
function getStarted() {
	$('#postcardFinance').modal('hide');
	updateLoadingText(alerts[lang]["alert-loggingin"], 0);
	var noshow = {
		'user': em,
		'noshow': $("#getStartedNoShow").is(':checked')
	};
	localStorage.setItem("noshow", JSON.stringify(noshow));
	userProductCheck(userid);
}

//  ------------------------------------------------------------------------------------------
function postcardAccept() {
	if ($("#newPass").val().length < 8) {
		showError("newPass", "tooshort");
		return false;
	}
	if ($("#newPass").val() != $("#newPassConfirm").val()) {
		showError("newPassConfirm", "passfail");
		return false;
	}
	pa = $("#newPass").val();
	$('#postcard').modal('hide');
	var noshow = {
		'user': em,
		'noshow': true
	};
	localStorage.setItem("noshow", JSON.stringify(noshow));
	updateUserPass();
}
//  ------------------------------------------------------------------------------------------
function updateUserPass() {
	updateLoadingText(alerts[lang]["alert-updateaccount"], 0);
	var timestamp = new Date();
	timestamp = timestamp.toISOString();
	var dt = {
		email: em,
		pass: pa,
		timestamp: timestamp
	};
	$.when(dataLoader('updatePass', JSON.stringify(dt)))
		.done(function (data) {
			goLogin();
		})
		.fail(function (data) {
			updateLoadingText(alerts[lang]["alert-noprocess"], 1, 0);
		});
}
//  ------------------------------------------------------------------------------------------
function createAcct(who) {
	updateLoadingText(alerts[lang]["alert-creatingaccount"], 0);
	var timestamp = new Date();
	timestamp = timestamp.toISOString();

	$.ajax({
		cache: false,
		type: "POST",
		contentType: 'application/json',
		url: siteURL + "/signup",

		data: JSON.stringify({
			otherInfo: metaData,
			user: {
				username: em,
				firstname: fn,
				lastname: ln,
				password: pa,
				locale: lang,
				private: pu
			},
			env: {
				returnUrl: siteURL + "?cl=" + clientName,
				networkId: network,
				timestamp: timestamp,
				channelId: ch
			}
		}),
		error: function (xhr, ajaxOptions, thrownError) {
			$('#loadingControl').modal('hide');
			if (xhr.statusCode == 404) {
				showError("sEmail", "createfail");
			} else {
				switch (xhr.statusText) {
					case "400 | user already in organization":
						showError("sEmail", "emailexists");
						break;
					default:
						showError("sEmail", "createfail");
						break;
				}
			}
		},
		success: function (data, statusText, xhr) {
			localStorage.setItem("gologin", "1");
			localStorage.setItem("lang", lang);
			localStorage.setItem("lastemail", em);
			localStorage.setItem("client", clientName);

			$("#email").val(em);
			userID = data.user.id;
			metaData = {};
			metaData["address"] = {};
			userNetworkProgramJoin();
		},
	});
}
//  ------------------------------------------------------------------------------------------
function userNetworkProgramJoin() {
	var dt = {
		user: {
			userId: userID,
			networkId: network,
			program: pro
		}
	};
	$.when(dataLoader('networkjoin', JSON.stringify(dt)))
		.done(function (data) {
			updateLoadingText(alerts[lang]["alert-confirmmessage"], 1);
		})
		.fail(function (data) {
			$('#loadingControl').modal('hide');
			showError("sEmail", "emailexists");
		});

}
//  ------------------------------------------------------------------------------------------
function goLoginCP() {
	var dt = {
		email: em,
		pass: pa
	};
	$.when(dataLoader('cplogin', JSON.stringify(dt)))
		.done(function (data) {
			if (data.data.authenticated === true) {
				$('#loadingControl').modal('hide');
				toggleModalWindow('postcard', 'show');
			} else {
				$('#loadingControl').modal('hide');
				showError("email", "userinvalid");
			}
		})
		.fail(function (data) {
			if (data.status > 399) {
				updateLoadingText(alerts[lang]["alert-noprocess"], 1);
			}
		});
}
//-----------------------------------------------------------------------
function goLogin() {
	updateLoadingText(alerts[lang]["alert-loggingin"], 0);
	var dt = {
		username: em,
		password: pa,
		organizationId: 10001
	};
	$.when(dataLoader('', JSON.stringify(dt), apiURL + '/auth/login'))
		.done(function (data) {
			localStorage.setItem("blnp.accessToken", data.accessToken);
			//-----------------------------------------------------------------------
			localStorage.setItem("lang", lang);
			localStorage.setItem("lastemail", em);
			localStorage.setItem("client", clientName);
			//-----------------------------------------------------------------------
			dt = data.accessToken.split(".");
			g = atob(dt[1]);
			g = JSON.parse(g);
			//if (lang === 'en') {
				userid = g.userId;
				var noshowchk = JSON.parse(localStorage.getItem("noshow"));
				if (noshowchk && noshowchk.user == em && noshowchk.noshow == true) {
					userProductCheck(userid);
				} else {
					$('#loadingControl').modal('hide');
					toggleModalWindow('postcardFinance', 'show');
				}
			//} else {
			//	userProductCheck(g.userId);
			//}
		})
		.fail(function (data) {
			$('#loadingControl').modal('hide');

			if (data.status == 401 || data.status == 400) {

				goLoginCP();
			} else if (data.status == 403) {
				$('#loadingControl').modal('hide');
				showError("email", "confirmmessage");
			} else {
				updateLoadingText(alerts[lang]["alert-noprocess"], 1);
			}
		});
}
//  ------------------------------------------------------------------------------------------
function networkSelect() {
	switch (lang) {
		case "en":
			clientSettings["name"] = "DreamBuilder English";
			clientSettings["channame"] = "DreamBuilder (English)";
			clientSettings["en-program_id"] = "enProgram";
			break;
		case "es":
			switch (clientSettings.country) {
				case "CL":
					clientSettings["name"] = "Dreambuilder Chile";
					clientSettings["channame"] = "Dreambuilder (Chile)";
					clientSettings["es-program_id"] = "clProgram";
					break;
				case "PE":
					clientSettings["name"] = "DreamBuilder Peru";
					clientSettings["channame"] = "DreamBuilder (Peru)";
					clientSettings["es-program_id"] = "peProgram";
					break;
				case "US":
					clientSettings["name"] = "DreamBuilder English";
					clientSettings["channame"] = "DreamBuilder (English)";
					clientSettings["es-program_id"] = "peProgram";
					break;
				default:
					clientSettings["name"] = "DreamBuilder Español";
					clientSettings["channame"] = "DreamBuilder (Español)";
					clientSettings["es-program_id"] = "peProgram";
					break;
			}
			break;
	}
	networkLookup('noinit');
}
///////////////////////////////////////////
function clientLoad() {
	updateLoadingText("", 0, 1);
	var dt = {
		client: clientName
	};
	$.when(dataLoader('clientLookup', JSON.stringify(dt))).done(function (data) {
		clientSettings = data;
		console.log("Client Settings: ", clientSettings);
		if (clientSettings.redirect) {
			clientName = clientSettings.redirect;
			clientLoad();
		}
		networkLookup();
	});
}
///////////////////////////////////////////
function networkLookup(act) {
	updateLoadingText("", 0, 1);
	var dt = {
		name: encodeURIComponent(clientSettings.name)
	};
	$.when(dataLoader('networkLookup', JSON.stringify(dt))).done(function (data) {
		if (data.length > 0) {
			var lookup = {};
			for (i = 0; i < data.length; i++) {
				lookup[data[i].name] = data[i];
			}
			clientSettings["network"] = lookup[clientSettings.name].id;
			getChannels(act);
		} else {
			updateLoadingText(alerts[lang]["alert-noload"], 1, 0);
		}
	});
}
///////////////////////////////////////////
function getChannels(act) {
	var dt = {
		id: ''
	};
	$.when(dataLoader('channelLookup', JSON.stringify(dt))).done(function (data) {
		if (data.length > 0) {
			var lookup = {};
			for (i = 0; i < data.length; i++) {
				lookup[data[i].name] = data[i];
			}
			clientSettings["channel_id"] = lookup[clientSettings.channame].id;
			if (lookup[clientSettings.channame].logo && clientSettings.logo == '') {
				clientSettings["logo"] = lookup[clientSettings.channame].logo.url;
			}
			$('#loadingControl').modal('hide');
			if (act == 'noinit') {
			} else {
				init();
			}
		} else {
			updateLoadingText(alerts[lang]["alert-noload"], 1, 0);
		}
	});
}
///////////////////////////////////////////
function userProductCheck(id) {
	var dt = {
		userId: id
	};
	$.when(dataLoader('userProgram', JSON.stringify(dt))).done(function (data) {
		//location.href = coreURL + 'program/' + data.item + '/activities?organization=dreambuilder';
		location.href = coreURL + '/program/' + data.item + '/activities';

	});
}

///////////////////////////////////////////
function dataLoader(ep, dt, url) {
	if (!url) {
		url = siteURL;
	}
	$.support.cors = true;
	return $.ajax({
		crossDomain: true,
		cache: false,
		contentType: "application/json; charset=utf-8",
		type: "POST",
		url: url + "/" + ep,
		dataType: "json",
		data: dt,
		error: function (xhr, ajaxOptions, thrownError) {
			lang = "en";
			if (localStorage.getItem("lang")) {
				lang = localStorage.getItem("lang");
			} 
			if (urlParams.lang) {
				lang = urlParams.lang;
			} else {
				lang = lang;
			}
			updateLoadingText(alerts[lang]["alert-accessdenied"], 1, 0);
		},
	});
}
