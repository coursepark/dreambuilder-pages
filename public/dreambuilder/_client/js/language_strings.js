var languageStrings = {
	"en": {
		"language": "English",
		"selecttext": "Please select",
		"regTitle": "Register Now",
		"loginTitle": "Login to Your Account",
		"navRegister": "Register",
		"navLogin": "Login",
		"firstname": "First Name",
		"lastname": "Last Name",
		"email": "Email Address",
		"password": "Password",
		"passwordlogin": "Password",
		"confirm": "Confirm Password",
		"terms": "I have read the Terms &amp; Conditions",
		"agree": "I agree to receive CoursePark emails containing news, updates and promotions regarding CoursePark products. Please note that you can withdraw your consent at any time.",
		"forgotpassword": "Forgot your password?",
		"sponsor0": "DreamBuilder has been made possible through funding by:",
		"sponsor1": "DreamBuilder was created by:",
		"sponsor2": "This website has been locally sponsored by:",
		"othermessage": "Type your country here",
		"countryselecttext": "Please enter a country name",
		"regionfilter": "Postal / ZIP Code",
		//"postcardtext": "<p>Hi!</p><p>I’m here in the new platform, waiting for you. I think you’re really going to like the new way it looks. First, you will need to accept the <a href='javascript:toggleModalWindow(\"termsconditions\");'>Terms and Conditions</a> and set a new password, just like you did when you first created your DreamBuilder account.</p><p>Then, you’ll be ready to continue on your journey!</p><p>See you there,<br> Alma</p>",
		"postcardtext": "<p>Welcome to the new and improved DreamBuilder! I think you're really going to like the new look.</p><p>First, you will need to accept the <a href='javascript:toggleModalWindow(\"termsconditions\");'>Terms and Conditions</a> and set a new password, just like you did when you first created your DreamBuilder account. This time, please make sure your password has 8 or more characters! </p><p>I am pleased to share the good news that the brand new <em><strong>Financing Your Dream</strong></em> course is now available under the <em><strong>My Programs tab</strong></em>. In this course, you will create a personalized Capital Action Plan to use as a roadmap to identify and obtain capital for your business. Be sure to allow two hours to complete this course. </p><p>Then you will be ready to continue on your journey.</p><p>See you there,<br> Alma</p>",
		"postcardFinancetext": "<p>Welcome Back to DreamBuilder! I am pleased to share the good news that the brand new <em><strong>Financing Your Dream</strong></em> course is now available under the <em><strong>My Programs</strong></em> tab. In this course, you will create a personalized Capital Action Plan to use as a roadmap to identify and obtain capital for your business.</p><p> Be sure to allow two hours to complete this course. Good luck!</p><p><em><small><label><input type='checkbox' id='getStartedNoShow'> Do not show this again</label></small></em></p>",
		"postcardNoticetext": "<p>Hello,</p><p>I'm sorry you can't login to continue your DreamBuilder journey, we know your training is important!  We need to perform maintenance updates to our DreamBuilder Business Plan Generator from September 26th - September  29th 2017.</p><p>If you want to create a new account, we have not disabled that functionality however you will not be able to login until the maintenance has been completed. </p><p>I'll see you back online to continue your training on Friday September 29th!</p>",
		"postcardvideo": "Learn about the new DreamBuilder",
		"postcardbutton": "Accept Terms & Conditions",
		"postcardFinancebutton": "Get Started",
		"postcardNoticebutton": "Close",
		"stateselect": "Select State",
		"termsTitle": "Terms & Conditions",
		"youtubeVideo": "geWfAfRGxC4",
		"extraNotice": "The U.S. Department of State requests this information pursuant to 22 U.S.C. 2651a so that it can administer your account and so that you can access the modules.  The information you provide may be made available as a routine use to contractors who have a need for the information to operate the service. For other routine uses of this information, please refer to State-79, Digital Outreach and Communications Records.  You do not have to provide the requested information; however, if you refuse, we will not be able to create an account for you and you will not be able to access the modules."
	},
	"es": {
		"language": "Español",
		"selecttext": "Por favor seleccione",
		"regTitle": "Registrarse Ahora",
		"loginTitle": "Iniciar sesión",
		"navRegister": "Registrarse",
		"navLogin": "Iniciar sesión",
		"firstname": "Su nombre",
		"lastname": "Su apellido",
		"email": "Su correo electrónico",
		"password": "Crear una contraseña",
		"passwordlogin": "Contraseña",
		"confirm": "Confirmar contraseña",
		"terms": "He leído los Términos y Condiciones",
		"agree": "Estoy de acuerdo en recibir correos electrónicos que contienen noticias, actualizaciones y promociones relativas a producto de CoursePark. Por favor, tenga en cuenta que puede retirar su consentimiento en cualquier momento.",
		"forgotpassword": "Olvidé mí contraseña",
		"sponsor0": "DreamBuilder ha sido posible gracias a la financiación por:",
		"sponsor1": "DreamBuilder fue creado por:",
		"sponsor2": "Este sitio web ha sido patrocinado localmente por:",
		"othermessage": "Escriba su país aquí",
		"countryselecttext": "Please ingrese un nombre de país",
		"regionfilter": "Código Postal / ZIP",
		"postcardFinancetext": "<p>¡Bienvenida de nuevo a Dreambuilder! Me complace compartir buenas noticias: El nuevo curso <em<strong>Financiando Su Sueño</strong></em> se encuentra ya disponible en la pestaña <strong>Mis programas</strong>.</p><p>En este curso usted creará un Plan de Acción de Capital personalizado que le servirá como una hoja de ruta para identificar y obtener capital para su negocio.</p><p>Asegúrese de disponer de tres horas para completar este curso. Si gusta dedicar más tiempo al curso, se puede regresar a ello como en el resto de los cursos de DreamBuilder.</p><p><strong>¡Buena Suerte!</strong></p><p><em><small><label><input type='checkbox' id='getStartedNoShow'> No muestres esto de nuevo</label></small></em></p>",
		"postcardtext": "<p>Bienvenida al Dreambuilder nuevo y mejorado! Creo que realmente te gustara la nueva apariencia.</p><p>Primero, tendrás que aceptar los <a href='javascript:toggleModalWindow(\"termsconditions\");'>Términos y Condiciones</a> y crear una nueva contraseña, igual que hicisteis cuando primero creasteis una cuenta en Dreambuilder. Esta vez, por favor asegúrate que tu contraseña tenga más que 8 caracteres!</p><p>Después que eso está hecho, estarás lista para continuar.</p><p>Te veré pronto!<br />Alma</p>",
		"postcardNoticetext":"<p>Hola,</p><p>¡Sentimos que no puedas iniciar sesión para continuar con tu viaje de DreamBuilder, sabemos que tu entrenamiento es importante!   Necesitamos realizar actualizaciones de mantenimiento de nuestro generador de planes de negocio DreamBuilder del 26 de septiembre al 29 de septiembre de 2017.  </p><p>Si desea crear una nueva cuenta, no hemos inhabilitado dicha funcionalidad, pero no podrá iniciar sesión hasta que se haya completado el mantenimiento. </p>",
		"postcardvideo": "Vea la nueva Dreambuilder",
		"postcardbutton": "Aceptar los Términos y Condiciones",
		"postcardFinancebutton": "Empezar",
		"stateselect": "Seleccione estado",
		"termsTitle": "Términos y Condiciones",
		"youtubeVideo": "jkVBTuur0vc",
		"postcardNoticebutton": "cerrar",
		"extraNotice": "El Departamento de Estado de los Estados Unidos solicita esta información de conformidad con la Sección 2651a del Título 22 del Código de los Estados Unidos con el fin de poder gestionar su cuenta y para que usted pueda tener acceso a los módulos. La información que nos proporcione podrá estar disponible para el uso habitual de contratistas que la necesitan para prestar servicios. Para otros usos habituales de esta información, sírvase consultar State-79, Digital Outreach and Communications Records. No es obligatorio que proporcione la información solicitada; sin embargo, si se niega a proporcionarla, no podremos crearle una cuenta y no podrá tener acceso a los módulos."

	}
};
var alerts = {
	"en": {
		"alert-email": "Please enter a valid email address.",
		"alert-bothnames": "Please enter your first and last name.",
		"alert-passfail": "Passwords do not match.",
		"alert-terms": "Please agree to the Terms and Conditions to continue.",
		"alert-reanyemail": "Please agree to receive CoursePark emails to continue. You can withdraw your consent at any time after your account has been created.",
		"alert-createfail": "Your account could not be created at this time.",
		"alert-exists": "An account with this email address address already exists.",
		"alert-tooshort": "Password must contain 1 uppercase character, 1 lowercase character, 1 number and be at least 8 characters long.",
		"alert-userinvalid": "Your account could not be verified, please check your Email and Password and try again.",
		"alert-langselect": "Please select your preferred language.",
		"alert-emailexists": "User already exists at this email address.",
		"alert-loginfail": "Invalid login provided.",
		"alert-creatingaccount": "Creating Account...",
		"alert-addingcertifications": "Adding Certifications...",
		"alert-addingcourses": "Adding Courses...",
		"alert-loggingin": "Logging In...",
		"alert-country": "Please enter your country before continuing.",
		"alert-confirmmessage": "An email has been sent to the address provided. Please confirm your account before proceeding.",
		"alert-noprocess": "We're sorry, your login could not be processed at this time.",
		"alert-noaccount": "We're sorry, we could not find an account associated with this email address. Please click the Register tab to set up your account. ",
		"alert-updateaccount": "Updating account...",
		"alert-redirect":"Redirecting...",
		"alert-validating":"Validating Account...",
		"alert-accessdenied":"We're sorry, your page could not be accessed. Please try again later.",
		"alert-noload":"We're sorry, your page could not be located. Please check the address and try again."
	},
	"es": {
		"alert-email": "Por favor ingrese una direccion de email valido.",
		"alert-bothnames": "Introduzca su nombre y apellido.",
		"alert-passfail": "Las contraseñas no son iguales.",
		"alert-terms": "Por favor acepte los Terminos y Condiciones de CoursePark para continuar",
		"alert-reanyemail": "Por favor, acepta recibir correos electrónicos de CoursePark para continuar. Usted puede retirar su consentimiento en cualquier momento después de que su cuenta ha sido creada",
		"alert-createfail": "Su cuenta no puede ser creada en este momento",
		"alert-exists": "Una cuenta con esta dirección de correo electrónico ya existe.",
		"alert-tooshort": "La contraseña debe contener 1 carácter en mayúscula, 1 carácter en minúscula, 1 número y tener al menos 8 caracteres.",
		"alert-userinvalid": "Su cuenta no pudo ser verificada. Por favor revise su Email y contraseña e intente de nuevo.",
		"alert-emailexists": "Usuario ya existe en esta dirección de correo electrónico.",
		"alert-loginfail": "Entrada no válida proporcionado.",
		"alert-creatingaccount": "Crear cuenta...",
		"alert-addingcertifications": "Adición Certificaciones...",
		"alert-addingcourses": "Adición de Cursos...",
		"alert-loggingin": "Inicio de sesión...",
		"alert-country": "Por favor, introduzca su país antes de continuar.",
		"alert-confirmmessage": "Un correo electrónico ha sido enviado a la dirección facilitada . Por favor, confirme su cuenta antes de proceder .",
		"alert-noprocess": "Lo sentimos , su inicio de sesión no se pudo procesar en este momento.",
		"alert-updateaccount": "Cuenta de actualización...",
		"alert-redirect":"Redireccionando...",
		"alert-validating":"Validar cuenta...",
		"alert-accessdenied":"Lo sentimos, no se pudo acceder a su página. Por favor, inténtelo de nuevo más tarde.",
		"alert-noload":"Lo sentimos, tu página no pudo ser localizada. Compruebe la dirección e inténtelo de nuevo."
	}
};
/////////////////////////////////////////////////////////////////////////
var library = {
	"languageStrings": {
		"en": {
			"select": "Please select a library branch"
		},
		"es": {
			"select": "Seleccione una sucursal de biblioteca"
		}
	},
	"alerts": {
		"en": {
			"alert-library": "Please make a selection"
		},
		"es": {
			"alert-library": "Por favor haga una selección"
		}
	},
	"storage": "library",
	"list": {
		"Acacia Library": "Acacia Library",
		"Agave Library": "Agave Library",
		"Burton Barr Central Library": "Burton Barr Central Library",
		"Century Library": "Century Library",
		"Cesar Chavez Library": "Cesar Chavez Library",
		"Cholla Library": "Cholla Library",
		"Desert Broom Library": "Desert Broom Library",
		"Desert Sage Library": "Desert Sage Library",
		"Harmon Library": "Harmon Library",
		"Ironwood Library": "Ironwood Library",
		"Juniper Library": "Juniper Library",
		"Mesquite Library": "Mesquite Library",
		"Ocotillo Library & Workforce Literacy Center": "Ocotillo Library & Workforce Literacy Center",
		"Palo Verde Library": "Palo Verde Library",
		"Saguaro Library": "Saguaro Library",
		"South Mountain Community Library": "South Mountain Community Library",
		"Yucca Library": "Yucca Library"
	}
};
/////////////////////////////////////////////////////////////////////////
var states = {
	"languageStrings": {
		"en": {
			"select": "Please select a state"
		},
		"es": {
			"select": "Seleccione estado"
		}
	},
	"storage": ["address", "region"],
	"alerts": {
		"en": {
			"alert-states": "Please make a selection"
		},
		"es": {
			"alert-states": "Por favor haga una selección"
		}
	},
	"list": {
		"Alabama": "AL",
		"Alaska": "AK",
		"American Samoa": "AS",
		"Arizona": "AZ",
		"Arkansas": "AR",
		"California": "CA",
		"Colorado": "CO",
		"Connecticut": "CT",
		"Delaware": "DE",
		"Dist. of Columbia": "DC",
		"Florida": "FL",
		"Georgia": "GA",
		"Guam": "GU",
		"Hawaii": "HI",
		"Idaho": "ID",
		"Illinois": "IL",
		"Indiana": "IN",
		"Iowa": "IA",
		"Kansas": "KS",
		"Kentucky": "KY",
		"Louisiana": "LA",
		"Maine": "ME",
		"Maryland": "MD",
		"Marshall Islands": "MH",
		"Massachusetts": "MA",
		"Michigan": "MI",
		"Micronesia": "FM",
		"Minnesota": "MN",
		"Mississippi": "MS",
		"Missouri": "MO",
		"Montana": "MT",
		"Nebraska": "NE",
		"Nevada": "NV",
		"New Hampshire": "NH",
		"New Jersey": "NJ",
		"New Mexico": "NM",
		"New York": "NY",
		"North Carolina": "NC",
		"North Dakota": "ND",
		"Northern Marianas": "MP",
		"Ohio": "OH",
		"Oklahoma": "OK",
		"Oregon": "OR",
		"Palau": "PW",
		"Pennsylvania": "PA",
		"Puerto Rico": "PR",
		"Rhode Island": "RI",
		"South Carolina": "SC",
		"South Dakota": "SD",
		"Tennessee": "TN",
		"Texas": "TX",
		"Utah": "UT",
		"Vermont": "VT",
		"Virginia": "VA",
		"Virgin Islands": "VI",
		"Washington": "WA",
		"West Virginia": "WV",
		"Wisconsin": "WI",
		"Wyoming": "WY"
	}
};
/////////////////////////////////////////////////////////////////////////
var memberFilters = {
		"standard": {
			"type": "text",
			"options": ['phone', 'id_number', 'city'],
			"languageStrings": {
				"en": {
					"member_phone": "Telephone Number",
					"member_id_number": "ID Number",
					"member_city": "City",
				},
				"es": {
					"member_phone": "Número de teléfono",
					"member_id_number": "Número de documento de identificación",
					"member_city": "Ciudad de residencia",
				}
			},
			"alerts": {
				"en": {
					"alert-phone": "Please enter your phone number",
					"alert-id_number": "Please enter your identification number",
					"alert-city": "Please enter your city",
				},
				"es": {
					"alert-phone": "Favor de ingresar su número de teléfono",
					"alert-id_number": "Favor de ingresar su número de identificación",
					"alert-city": "Favor indique su ciudad de residencia",
				}
			}
		},
		"gender": {
			"type": "radio",
			"label": "Gender",
			"options": ['male', 'female', 'not_say'],
			"languageStrings": {
				"en": {
					"member_male": "Male",
					"member_female": "Female",
					"member_not_say": "Prefer not to say",
				},
				"es": {
					"member_male": "Masculino",
					"member_female": "Hembra ",
					"member_not_say": "Prefiero no decirlo",
				}
			},
			"alerts": {
				"en": {
					"alert-Gender": "Please make a selection"
				},
				"es": {
					"alert-Gender": "Por favor haga una selección"
				}
			}
		},
	}
	/////////////////////////////////////////////////////////////////////////
var sernamSelector = {
		"defaults": {
			//"channel_id": 10020,
			//"network": 42331,
		},
		"languageStrings": {
			"en": {
				"selecttext": "Select Region"
			},
			"es": {
				"selecttext": "Seleccione su región"
			}
		},
		"alerts": {
			"en": {
				"alert-sernamSelector": "Please make a selection"
			},
			"es": {
				"alert-sernamSelector": "Por favor haga una selección"
			}
		},
		"storage": "locality",
		"options": {
			"Arica y Parinacota": {
				"name": "Arica y Parinacota",
				"channame": "Arica y Parinacota"
			},
			"Tarapacá": {
				"name": "Tarapacá",
				"channame": "DreamBuilder (Tarapacá)"
			},
			"Sernam Antofagasta": {
				"name": "Sernam Antofagasta",
				"channame": "DreamBuilder (Sernam Antofagasta)"
			},
			"Sernam Atacama": {
				"name": "Sernam Atacama",
				"channame": "Sernam Atacama"
			},
			"Coquimbo": {
				"name": "Coquimbo",
				"channame": "DreamBuilder (Coquimbo)"
			},
			"Valparaíso": {
				"name": "Valparaíso",
				"channame": "DreamBuilder (Valparaíso)"
			},
			"Metropolitana de Santiago": {
				"name": "Metropolitana de Santiago",
				"channame": "Metropolitana de Santiago"
			},
			"O'Higgins": {
				"name": "O'Higgins",
				"channame": "O'Higgins"
			},
			"Maule": {
				"name": "Maule",
				"channame": "DreamBuilder (Maule)"
			},
			"Ñuble": {
				"name": "Ñuble",
				"channame": "DreamBuilder (Ñuble)"
			},
			"Biobío": {
				"name": "Biobío",
				"channame": "DreamBuilder (Biobío)"
			},
			"La Araucanía": {
				"name": "La Araucanía",
				"channame": "DreamBuilder (La Araucanía)"
			},
			"Los Ríos": {
				"name": "Los Ríos",
				"channame": "DreamBuilder (Los Ríos)"
			},
			"Los Lagos": {
				"name": "Los Lagos",
				"channame": "DreamBuilder (Los Lagos)"
			},
			"Aysén": {
				"name": "Aysén",
				"channame": "DreamBuilder (Aysén)"
			},
			"Magallanes": {
				"name": "Magallanes",
				"channame": "DreamBuilder (Magallanes)"
			}
		}
	}
	/////////////////////////////////////////////////////////////////////////
var programSelector = {
		"defaults": {
			//"channel_id": 10020,
			//"network": 42331,
		},
		"callback": "countrySelectChange",
		"languageStrings": {
			"en": {
				"selecttext": "Select your country"
			},
			"es": {
				"selecttext": "Seleccione su país"
			}
		},
		"alerts": {
			"en": {
				"alert-programSelector": "Please make a selection"
			},
			"es": {
				"alert-programSelector": "Por favor haga una selección"
			}
		},
		//"storage": "country-name",
		"options": {
			"Chile": {
				"es-program_id": "clProgram",
				"country": "CL"
			},
			"Peru": {
				"country": "PE"
			},
			"United States": {
				"country": "US"
			},
			"--------------": {
				"country": 0
			},
			"Argentina": {
				"country": "AR"
			},
			"Belize": {
				"country": "BZ"
			},
			"Boliva": {
				"country": "BO"
			},
			"Brazil": {
				"country": "BR"
			},
			"Colombia": {
				"country": "CO"
			},
			"Costa Rica": {
				"country": "CR"
			},
			"Cuba": {
				"country": "CU"
			},
			"Dominican Republic": {
				"country": "DO"
			},
			"Ecuador": {
				"country": "EC"
			},
			"El Salvador": {
				"country": "SV"
			},
			"Guatemala": {
				"country": "GT"
			},
			"Honduras": {
				"country": "HN"
			},
			"Mexico": {
				"country": "MX"
			},
			"Nicaragua": {
				"country": "NI"
			},
			"Panama": {
				"country": "PA"
			},
			"Paraguay": {
				"country": "PY"
			},
			"Puerto Rico": {
				"country": "PR"
			},
			"Uruguay": {
				"country": "UY"
			},
			"Venezuela": {
				"country": "VE"
			},
			"---------------": {
				"country": 0
			},
			"Spain": {
				"country": "ES"
			},
			"Canada": {
				"country": "CA"
			},
			"Haiti": {
				"country": "HT"
			},
			"Other / Otro": {
				"country": "XX"
			}
		}
	}
	/////////////////////////////////////////////////////////////////////////
var maraSelector = {
		"defaults": {
			//"channel_id": 10020,
			//"network": 42331,
		},
		"callback": "countrySelectChange",
		"languageStrings": {
			"en": {
				"selecttext": "Select your country"
			},
			"es": {
				"selecttext": "Seleccione su país"
			}
		},
		"alerts": {
			"en": {
				"alert-maraSelector": "Please make a selection"
			},
			"es": {
				"alert-maraSelector": "Por favor haga una selección"
			}
		},
		//"storage": "country-name",
		"options": {
			"Nigeria": {
				"country": "NG"
			},
			"South Africa": {
				"country": "ZA"
			},
			"Kenya": {
				"country": "KE"
			},
			"Uganda": {
				"country": "UG"
			},
			"Ghana": {
				"country": "GH"
			},
			"India": {
				"country": "IN"
			},
			"United States": {
				"country": "US"
			},
			"Zambia": {
				"country": "ZM"
			},
			"United Kingdom": {
				"country": "GB"
			},
			"Tanzania": {
				"country": "TZ"
			},
			"Other / Otro": {
				"country": "XX"
			}
		}
	}
	//////////////////////////////////////////////////////////////////////
var sercotecSelector = {
		"defaults": {
			//"channel_id": 10020,
			//"network": 42331,
		},
		"languageStrings": {
			"en": {
				"selecttext": "Select Region"
			},
			"es": {
				"selecttext": "Seleccione su región"
			}
		},
		"alerts": {
			"en": {
				"alert-sercotecSelector": "Please make a selection"
			},
			"es": {
				"alert-sercotecSelector": "Por favor haga una selección"
			}
		},
		"storage": "locality",
		"options": {
			"I-Tarapacá - Centro Iquique": {
				"name": "I-Tarapacá - Centro Iquique",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"I-Tarapacá - Centro Pozo Almonte": {
				"name": "I-Tarapacá - Centro Pozo Almonte",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"II-Antofagasta - Centro Antofagasta": {
				"name": "II-Antofagasta - Centro Antofagasta",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"II-Antofagasta - Centro Calama": {
				"name": "II-Antofagasta - Centro Calama",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"II-Antofagasta - Centro Tocopilla": {
				"name": "II-Antofagasta - Centro Tocopilla",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"III-Atacama - Centro Copiapó": {
				"name": "III-Atacama - Centro Copiapó",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"III-Atacama - Centro Vallenar": {
				"name": "III-Atacama - Centro Vallenar",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"IV-Coquimbo - Centro La Serena": {
				"name": "IV-Coquimbo - Centro La Serena",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"IV-Coquimbo - Centro Ovalle": {
				"name": "IV-Coquimbo - Centro Ovalle",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"IV-Coquimbo- Centro Illapel": {
				"name": "IV-Coquimbo - Centro Illapel",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"V-Valparaíso - Centro Quillota": {
				"name": "V-Valparaíso - Centro Quillota",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"V-Valparaíso - Centro Valparaíso": {
				"name": "V-Valparaíso - Centro Valparaíso",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"V-Valparaíso - Centro San Antonio": {
				"name": "V-Valparaíso - Centro San Antonio",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"V-Valparaíso - Centro San Felipe": {
				"name": "V-Valparaíso - Centro San Felipe",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"V-Valparaíso - Centro Aconcagua": {
				"name": "V-Valparaíso - Centro Aconcagua",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"VI-O’Higgins - Centro Santa Cruz": {
				"name": "VI-O’Higgins - Centro Santa Cruz",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"VI-O’Higgins - Centro Rancagua": {
				"name": "VI-O’Higgins - Centro Rancagua",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"VI-O’Higgins - Centro Pichilemu": {
				"name": "VI-O’Higgins - Centro Pichilemu",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"VI-O’Higgins - Centro San Fernando": {
				"name": "VI-O’Higgins - Centro San Fernando",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"VII-Maule - Centro Cauquenes": {
				"name": "VII-Maule - Centro Cauquenes",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"VII-Maule - Centro Talca": {
				"name": "VII-Maule - Centro Talca",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"VII-Maule - Centro Curicó": {
				"name": "VII-Maule - Centro Curicó",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"VII-Maule - Centro Linares": {
				"name": "VII-Maule - Centro Linares",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"VIII-Biobío - Centro Cañete": {
				"name": "VIII-Biobío - Centro Cañete",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			
			"VIII-Biobío - Centro Los Ángeles": {
				"name": "VIII-Biobío - Centro Los Ángeles",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"VIII-Biobío - Centro Concepción": {
				"name": "VIII-Biobío - Centro Concepción",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"IX-Araucanía- Centro Angol": {
				"name": "IX-Araucanía - Centro Angol",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"IX-Araucanía - Centro Temuco": {
				"name": "IX-Araucanía - Centro Temuco",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"IX-Araucanía - Centro Villarrica": {
				"name": "IX-Araucanía - Centro Villarrica",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"X-Los Lagos - Centro Osorno": {
				"name": "X-Los Lagos - Centro Osorno",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"X-Los Lagos - Centro Puerto Montt": {
				"name": "X-Los Lagos - Centro Puerto Montt",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"X-Los Lagos - Centro Chiloé": {
				"name": "X-Los Lagos - Centro Chiloé",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"XI-Aysén - Centro Aysén": {
				"name": "XI-Aysén - Centro Aysén",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"XI-Aysén - Centro Coyhaique": {
				"name": "XI-Aysén - Centro Coyhaique",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"XII-Magallanes - Centro Puerto Natales": {
				"name": "XII-Magallanes - Centro Puerto Natales",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"XII-Magallanes - Centro Punta Arenas": {
				"name": "XII-Magallanes - Centro Punta Arenas",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"XIV-Los Ríos - Centro La Unión": {
				"name": "XIV-Los Ríos - Centro La Unión",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"XIV-Los Ríos - Centro Valdivia": {
				"name": "XIV-Los Ríos - Centro Valdivia",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"XV-Arica Parinacota - Centro Arica ": {
				"name": "XV-Arica Parinacota - Centro Arica",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"XVI-Ñuble - Centro Chillán": {
				"name": "XVI-Ñuble - Centro Chillán",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"RM-Metropolitana- Centro Melipilla": {
				"name": "RM-Metropolitana - Centro Melipilla",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"RM-Metropolitana - Centro Independencia": {
				"name": "RM-Metropolitana - Centro Independencia",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"RM-Metropolitana- Centro San Bernardo": {
				"name": "RM-Metropolitana - Centro San Bernardo",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"RM-Metropolitana - Centro Santiago": {
				"name": "RM-Metropolitana - Centro Santiago",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"RM-Metropolitana - Centro La Florida": {
				"name": "RM-Metropolitana - Centro La Florida",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"RM-Metropolitana - Centro Maipú": {
				"name": "RM-Metropolitana - Centro Maipú",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"RM-Metropolitana - Centro Talagante": {
				"name": "RM-Metropolitana - Centro Talagante",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"RM-Metropolitana - Centro Ñuñoa": {
				"name": "RM-Metropolitana - Centro Ñuñoa",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"RM-Metropolitana - Centro Quilicura": {
				"name": "RM-Metropolitana - Centro Quilicura",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"RM-Metropolitana - Centro Pudahuel": {
				"name": "RM-Metropolitana - Centro Pudahuel",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"RM-Metropolitana - Centro Las Condes": {
				"name": "RM-Metropolitana - Centro Las Condes",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"RM-Metropolitana- Centro Estación Central": {
				"name": "RM-Metropolitana - Centro Estación Central",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"RM-Metropolitana - Colina": {
				"name": "RM-Metropolitana - Colina",
				"channame": "Dreambuilder (Sercotec Chile)"
			},
			"RM-Metropolitana - Centro Puente Alto": {
				"name": "RM-Metropolitana - Centro Puente Alto",
				"channame": "Dreambuilder (Sercotec Chile)"
			}
		}
	}
	///////
var amspacesSelector = {
		"defaults": {
			//"channel_id": 10020,
			//"network": 42331,
		},
		"callback": "amSpacesSelectChange",
		"languageStrings": {
			"en": {
				"selecttext": "Select your country"
			},
			"es": {
				"selecttext": "Seleccione su país"
			}
		},
		"alerts": {
			"en": {
				"alert-amspacesSelector": "Please make a selection"
			},
			"es": {
				"alert-amspacesSelector": "Por favor haga una selección"
			}
		},
		//"storage": "country-name",
		"options": {
			"Chile": {
				"es-program_id": "clProgram",
				"country": "CL"
			},
			"Peru": {
				"country": "PE"
			},
			"United States": {
				"country": "US"
			},
			"--------------": {
				"country": 0
			},
			"Argentina": {
				"country": "AR"
			},
			"Belize": {
				"country": "BZ"
			},
			"Boliva": {
				"country": "BO"
			},
			"Brazil": {
				"country": "BR"
			},
			"Colombia": {
				"country": "CO"
			},
			"Costa Rica": {
				"country": "CR"
			},
			"Cuba": {
				"country": "CU"
			},
			"Dominican Republic": {
				"country": "DO"
			},
			"Ecuador": {
				"country": "EC"
			},
			"El Salvador": {
				"country": "SV"
			},
			"Guatemala": {
				"country": "GT"
			},
			"Honduras": {
				"country": "HN"
			},
			"Mexico": {
				"country": "MX"
			},
			"Nicaragua": {
				"country": "NI"
			},
			"Panama": {
				"country": "PA"
			},
			"Paraguay": {
				"country": "PY"
			},
			"Puerto Rico": {
				"country": "PR"
			},
			"Uruguay": {
				"country": "UY"
			},
			"Venezuela": {
				"country": "VE"
			},
			"---------------": {
				"country": 0
			},
			"Spain": {
				"country": "ES"
			},
			"Canada": {
				"country": "CA"
			},
			"Haiti": {
				"country": "HT"
			},
			"Other / Otro": {
				"country": "XX"
			}
		}
	}
	/////////////////////////////////////////////////////////////////////////
var mvsSelector = {
	"defaults": {
		//"channel_id": 10020,
		//"network": 42331,
	},
	"callback": "mvsSelectChange",
	"languageStrings": {
		"en": {
			"selecttext": "Select your country"
		},
		"es": {
			"selecttext": "Seleccione su país"
		}
	},
	"alerts": {
		"en": {
			"alert-mvsSelector": "Please make a selection"
		},
		"es": {
			"alert-mvsSelector": "Por favor haga una selección"
		}
	},
	//"storage": "country-name",
	"options": {
		"Chile": {
			"es-program_id": "clProgram",
			"country": "CL"
		},
		"Peru": {
			"es-program_id": "peProgram",
			"country": "PE"
		},
		"United States": {
			"country": "US"
		},
		"--------------": {
			"country": 0
		},
		"Argentina": {
			"country": "AR"
		},
		"Belize": {
			"country": "BZ"
		},
		"Boliva": {
			"country": "BO"
		},
		"Brazil": {
			"country": "BR"
		},
		"Colombia": {
			"country": "CO"
		},
		"Costa Rica": {
			"country": "CR"
		},
		"Cuba": {
			"country": "CU"
		},
		"Dominican Republic": {
			"country": "DO"
		},
		"Ecuador": {
			"country": "EC"
		},
		"El Salvador": {
			"country": "SV"
		},
		"Guatemala": {
			"country": "GT"
		},
		"Honduras": {
			"country": "HN"
		},
		"Mexico": {
			"country": "MX"
		},
		"Nicaragua": {
			"country": "NI"
		},
		"Panama": {
			"country": "PA"
		},
		"Paraguay": {
			"country": "PY"
		},
		"Puerto Rico": {
			"country": "PR"
		},
		"Uruguay": {
			"country": "UY"
		},
		"Venezuela": {
			"country": "VE"
		},
		"---------------": {
			"country": 0
		},
		"Spain": {
			"country": "ES"
		},
		"Canada": {
			"country": "CA"
		},
		"Haiti": {
			"country": "HT"
		},
		"Other / Otro": {
			"country": "XX"
		}
	}
}
	/////////////////////////////////////////////////////////////////////////
	var utahSelector = {
		"defaults": {
			//"channel_id": 10020,
			//"network": 42331,
		},
		"callback": "utahSelectChange",
		"languageStrings": {
			"en": {
				"selecttext": "Select your region"
			},
			"es": {
				"selecttext": "Seleccione su región"
			}
		},
		"alerts": {
			"en": {
				"alert-utahSelector": "Please make a selection"
			},
			"es": {
				"alert-utahSelector": "Por favor haga una selección"
			}
		},
		//"storage": "country-name",
		"options": {
			"Northern WBCUtah Client": {
				"name": "Northern WBCUtah Client",
				"channel": "northernwbcutahclient"
			},
			"Southern/Eastern WBC Utah Client": {
				"name": "Southern/Eastern WBCUtah Client",
				"channel": "southerneasternwbcutahclient"
			}
		}
	}

	var aweSelector = {
		"defaults": {
			//"channel_id": 10020,
			//"network": 42331,
		},
		"callback": "aweSelectChange",
		"languageStrings": {
			"en": {
				"selecttext": "Select your country"
			},
			"es": {
				"selecttext": "Seleccione su país"
			}
		},
		"alerts": {
			"en": {
				"alert-aweSelector": "Please make a selection"
			},
			"es": {
				"alert-aweSelector": "Por favor haga una selección"
			}
		},
		//"storage": "country-name",
		"options": {
			"Bahamas": {
				"network": "46623",
				"country": "BS"
			},
			"Barbados": {
				"network": "46625",
				"country": "BB"
			},
			"Chile": {
				"network": "46624",
				"country": "CL"
			},
			"Colombia": {
				"network": "46626",
				"country": "CO"
			},
			"Costa Rica": {
				"network": "46627",
				"country": "CR"
			},
			"Dominican Republic": {
				"network": "46628",
				"country": "DO"
			},
			"Ecuador": {
				"network": "46629",
				"country": "EC"
			},
			"El Salvador": {
				"network": "46630",
				"country": "SV"
			},
			"Ghana": {
				"network": "46637",
				"country": "GH"
			},
			"Guatemala": {
				"network": "46631",
				"country": "GT"
			},
			"Haiti": {
				"network": "46632",
				"country": "HT"
			},
			"Honduras": {
				"network": "46633",
				"country": "HN"
			},
			"Jamaica": {
				"network": "46634",
				"country": "JM"
			},
			"Kenya": {
				"network": "46638",
				"country": "KE"
			},
			"Nigeria": {
				"network": "46639",
				"country": "NG"
			},
			"Papua New Guinea": {
				"network": "46648",
				"country": "PG"
			},
			"Peru": {
				"country": "PE",
				"network": "46635",
			},
			"Rwanda": {
				"country": "RW",
				"network": "46640",
			},
			"Senegal": {
				"country": "SN",
				"network": "46641",
			},
			"South Africa": {
				"country": "ZA",
				"network": "46642",
			},
			"Spain": {
				"country": "ES",
				"network": "46647",
			},
			"Tanzania": {
				"country": "TZ",
				"network": "46643",
			},
			"Uganda": {
				"country": "UG",
				"network": "46644",
			},
			"Venezuela": {
				"country": "VE",
				"network": "46636",
			},
			"Zambia": {
				"country": "ZM",
				"network": "46645",
			},
			"Zimbabwe": {
				"country": "ZW",
				"network": "46646",
			},
			// "Other / Otro": {
			// 	"network": "46622",
			// 	"country": "XX"
			// }
		}
	};
	/////////////////////////////////////////////////////////////////////////
	var cenproSelector = {
		"defaults": {
			//"channel_id": 10020,
			//"network": 42331,
		},
		"callback": "cenproSelectChange",
		"languageStrings": {
			"en": {
				"selecttext": "Select your country"
			},
			"es": {
				"selecttext": "Seleccione su país"
			}
		},
		"alerts": {
			"en": {
				"alert-cenproSelector": "Please make a selection"
			},
			"es": {
				"alert-cenproSelector": "Por favor haga una selección"
			}
		},
		//"storage": "country-name",
		"options": {
			"Belice": {
				"country": "BZ"
			},
			"Costa Rica": {
				"country": "CR"
			},
			"El Salvador": {
				"country": "SV"
			},
			"Guatemala": {
				"country": "GT"
			},
			"Honduras": {
				"country": "HN"
			},
			"Panamá": {
				"country": "PA",
			},
			"República Dominicana": {
				"country": "DO"
			},
		}
	};
/////////////////////////////////////////////////////////////////////////
var prodemuSelector = {
	"defaults": {
		//"channel_id": 10020,
		//"network": 42331,
	},
	// "callback": "cenproSelectChange",
	"languageStrings": {
		"en": {
			"selecttext": "Select your country"
		},
		"es": {
			"selecttext": "Seleccione su país"
		}
	},
	"alerts": {
		"en": {
			"alert-prodemuSelector": "Please make a selection"
		},
		"es": {
			"alert-prodemuSelector": "Por favor haga una selección"
		}
	},
	//"storage": "country-name",
	"options": {
		"REGION DE ARICA-PARINACOTA-ARICA": {
			"name": "REGION DE ARICA-PARINACOTA-ARICA"
		},
		"REGION DE ARICA-PARINACOTA-PARINACOTA": {
			"name": "REGION DE ARICA-PARINACOTA-PARINACOTA"
		},
		"REGION DE TARAPACA-IQUIQUE/TAMARUGAL": {
			"name": "REGION DE TARAPACA-IQUIQUE/TAMARUGAL"
		},
		"REGION DE ANTOFAGASTA-ANTOFAGASTA": {
			"name": "REGION DE ANTOFAGASTA-ANTOFAGASTA"
		},
		"REGION DE ANTOFAGASTA-TOCOPILLA": {
			"name": "REGION DE ANTOFAGASTA-TOCOPILLA"
		},
		"REGION DE ANTOFAGASTA-EL LOA (CALAMA)": {
			"name": "REGION DE ANTOFAGASTA-EL LOA (CALAMA)"
		},
		"REGION DE ATACAMA-COPIAPO": {
			"name": "REGION DE ATACAMA-COPIAPO"
		},
		"REGION DE ATACAMA-CHAÑARAL": {
			"name": "REGION DE ATACAMA-CHAÑARAL"
		},
		"REGION DE ATACAMA-HUASCO": {
			"name": "REGION DE ATACAMA-HUASCO"
		},
		"REGION DE COQUIMBO-ELQUI": {
			"name": "REGION DE COQUIMBO-ELQUI"
		},
		"REGION DE COQUIMBO-LIMARI": {
			"name": "REGION DE COQUIMBO-LIMARI"
		},
		"REGION DE COQUIMBO-CHOAPA": {
			"name": "REGION DE COQUIMBO-CHOAPA"
		},
		"REGION DE VALPARAISO-VALPARAISO": {
			"name": "REGION DE VALPARAISO-VALPARAISO"
		},
		"REGION DE VALPARAISO-MARGA-MARGA": {
			"name": "REGION DE VALPARAISO-MARGA-MARGA"
		},
		"REGION DE VALPARAISO-PETORCA": {
			"name": "REGION DE VALPARAISO-PETORCA"
		},
		"REGION DE VALPARAISO-LOS ANDES": {
			"name": "REGION DE VALPARAISO-LOS ANDES"
		},
		"REGION DE VALPARAISO-SAN FELIPE": {
			"name": "REGION DE VALPARAISO-SAN FELIPE"
		},
		"REGION DE VALPARAISO-QUILLOTA": {
			"name": "REGION DE VALPARAISO-QUILLOTA"
		},
		"REGION DE VALPARAISO-SAN ANTONIO": {
			"name": "REGION DE VALPARAISO-SAN ANTONIO"
		},
		"REGION DE VALPARAISO-I. DE PASCUA": {
			"name": "REGION DE VALPARAISO-I. DE PASCUA"
		},
		"REGION DE O'HIGGINS-CACHAPOAL": {
			"name": "REGION DE O'HIGGINS-CACHAPOAL"
		},
		"REGION DE O'HIGGINS-CARDENAL CARO": {
			"name": "REGION DE O'HIGGINS-CARDENAL CARO"
		},
		"REGION DE O'HIGGINS-COLCHAGUA": {
			"name": "REGION DE O'HIGGINS-COLCHAGUA"
		},
		"REGION DEL MAULE-TALCA": {
			"name": "REGION DEL MAULE-TALCA"
		},
		"REGION DEL MAULE-LINARES": {
			"name": "REGION DEL MAULE-LINARES"
		},
		"REGION DEL MAULE-CURICO": {
			"name": "REGION DEL MAULE-CURICO"
		},
		"REGION DEL MAULE-CAUQUENES": {
			"name": "REGION DEL MAULE-CAUQUENES"
		},
		"REGION DEL BIO BIO-CONCEPCION": {
			"name": "REGION DEL BIO BIO-CONCEPCION"
		},
		"REGION DEL BIO BIO-BIO BIO": {
			"name": "REGION DEL BIO BIO-BIO BIO"
		},
		"REGION DEL BIO BIO-ARAUCO": {
			"name": "REGION DEL BIO BIO-ARAUCO"
		},
		"REGION DE ÑUBLE-ÑUBLE": {
			"name": "REGION DE ÑUBLE-ÑUBLE"
		},
		"REGION DE ÑUBLE-DIGUILLIN": {
			"name": "REGION DE ÑUBLE-DIGUILLIN"
		},
		"REGION DE TEMUCO-CAUTIN": {
			"name": "REGION DE TEMUCO-CAUTIN"
		},
		"REGION DE TEMUCO-MALLECO": {
			"name": "REGION DE TEMUCO-MALLECO"
		},
		"REGION DE LOS RIOS-VALDIVIA": {
			"name": "REGION DE LOS RIOS-VALDIVIA"
		},
		"REGION DE LOS RIOS-RANCO": {
			"name": "REGION DE LOS RIOS-RANCO"
		},
		"REGION DE LOS LAGOS-LLANQUIHUE": {
			"name": "REGION DE LOS LAGOS-LLANQUIHUE"
		},
		"REGION DE LOS LAGOS-OSORNO": {
			"name": "REGION DE LOS LAGOS-OSORNO"
		},
		"REGION DE LOS LAGOS-CHILOE": {
			"name": "REGION DE LOS LAGOS-CHILOE"
		},
		"REGION GRAL. CARLOS IBAÑEZ DEL CAMPO-COYHAIQUE": {
			"name": "REGION GRAL. CARLOS IBAÑEZ DEL CAMPO-COYHAIQUE"
		},
		"REGION GRAL. CARLOS IBAÑEZ DEL CAMPO-AYSEN": {
			"name": "REGION GRAL. CARLOS IBAÑEZ DEL CAMPO-AYSEN"
		},
		"REGION GRAL. CARLOS IBAÑEZ DEL CAMPO-GRAL CARRERA": {
			"name": "REGION GRAL. CARLOS IBAÑEZ DEL CAMPO-GRAL CARRERA"
		},
		"REGION GRAL. CARLOS IBAÑEZ DEL CAMPO-CAP. PRAT": {
			"name": "REGION GRAL. CARLOS IBAÑEZ DEL CAMPO-CAP. PRAT"
		},
		"REGION DE MAGALLANES-MAGALLANES": {
			"name": "REGION DE MAGALLANES-MAGALLANES"
		},
		"REGION DE MAGALLANES-TIERRA DEL FUEGO": {
			"name": "REGION DE MAGALLANES-TIERRA DEL FUEGO"
		},
		"REGION DE MAGALLANES-ULTIMA ESPERANZA": {
			"name": "REGION DE MAGALLANES-ULTIMA ESPERANZA"
		},
		"REGION METROPOLITANA-STGO. SUR 1": {
			"name": "REGION METROPOLITANA-STGO. SUR 1"
		},
		"REGION METROPOLITANA-STGO. SUR 2": {
			"name": "REGION METROPOLITANA-STGO. SUR 2"
		},
		"REGION METROPOLITANA-STGO. NORTE 1": {
			"name": "REGION METROPOLITANA-STGO. NORTE 1"
		},
		"REGION METROPOLITANA-SANTIAGO NORTE 2": {
			"name": "REGION METROPOLITANA-SANTIAGO NORTE 2"
		},
		"REGION METROPOLITANA-DEL MAIPO": {
			"name": "REGION METROPOLITANA-DEL MAIPO"
		},
		"REGION METROPOLITANA-CORDILLERA": {
			"name": "REGION METROPOLITANA-CORDILLERA"
		},
		"REGION METROPOLITANA-CHACABUCO": {
			"name": "REGION METROPOLITANA-CHACABUCO"
		},
		"REGION METROPOLITANA-TALAGANTE": {
			"name": "REGION METROPOLITANA-TALAGANTE"
		},
		"REGION METROPOLITANA-MELIPILLA": {
			"name": "REGION METROPOLITANA-MELIPILLA"
		},
	}
};
/////////////////////////////////////////////////////////////////////////