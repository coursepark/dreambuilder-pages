var clientSettingsLoad = {
    "candelaria": {
        "network": 42259,
        "channel_id": 10013,
        "include_langs": ["es"],
        "en-program_id": 10001,
        "es-program_id": 10000,
        "logo": "candelaria.jpg",
        "country": "CL",
        "private_profile": false,
        "extLink": "",
        "footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
        "member_filters": "standard",
        "region_filter": false,
        "services": ["login", "reg"],
        "template": "template-general.html",
        "styles": ""
    },
    "aweec": {
        "network": 42322,
        "channel_id": 10036,
        "include_langs": ["en", "es"],
        "en-program_id": 10001,
        "es-program_id": 10002,
        "logo": "aweec-logo.jpg",
        "country": "US",
        "private_profile": false,
        "extLink": "",
        "footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
        "member_filters": false,
        "region_filter": "states",
        "services": ["login", "reg"],
        "template": "template-general.html",
        "styles": "",
        "network_select": {
            "languageStrings": {
                "en": {
                    "network_select": "Please select..."
                },
                "es": {
                    "network_select": "Seleccione su país ..."
                }
            },
            "alerts": {
                "en": {
                    "network": "Please enter your country before continuing."
                },
                "es": {
                    "network": "Por favor, introduzca su país antes de continuar."
                }
            }
        }
    },
    "perucamaras": {
        "network": 42301,
        "channel_id": 10040,
        "include_langs": ["es"],
        "en-program_id": 10001,
        "es-program_id": 10002,
        "logo": "ccl-logo.jpg",
        "country": "PE",
        "private_profile": false,
        "extLink": "",
        "footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
        "member_filters": false,
        "region_filter": false,
        "services": ["login", "reg"],
        "template": "template-general.html",
        "styles": ""
    },
    "sba": {
        "network": 42275,
        "channel_id": 3,
        "include_langs": ["en", "es"],
        "en-program_id": 10001,
        "es-program_id": 10002,
        "logo": "sba-logo-2.jpg",
        "country": "PE",
        "private_profile": true,
        "extLink": "https://www.sba.gov/",
        "footers": ["sp-logo-freeport.jpg", "sp-logo-thunderbird.jpg"],
        "member_filters": false,
        "region_filter": true,
        "services": ["login", "reg"],
        "template": "template-general.html",
        "styles": ""
    },
};