var country_list = ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua &amp; Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia &amp; Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Chad", "Chile", "China", "Colombia", "Congo", "Cook Islands", "Costa Rica", "Cote D Ivoire", "Croatia", "Cruise Ship", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kuwait", "Kyrgyz Republic", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Mauritania", "Mauritius", "Mexico", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Pakistan", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre &amp; Miquelon", "Samoa", "San Marino", "Satellite", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "South Africa", "South Korea", "Spain", "Sri Lanka", "St Kitts &amp; Nevis", "St Lucia", "St Vincent", "St. Lucia", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor L'Este", "Togo", "Tonga", "Trinidad &amp; Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks &amp; Caicos", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States of America", "Uruguay", "Uzbekistan", "Venezuela", "Vietnam", "Virgin Islands (US)", "Yemen", "Zambia", "Zimbabwe"];
var isoCountries = [

     {
        value: 'Afghanistan',
        data: 'AF'
    }, {
        value: 'Aland Islands',
        data: 'AX'
    }, {
        value: 'Albania',
        data: 'AL'
    }, {
        value: 'Algeria',
        data: 'DZ'
    }, {
        value: 'American Samoa',
        data: 'AS'
    }, {
        value: 'Andorra',
        data: 'AD'
    }, {
        value: 'Angola',
        data: 'AO'
    }, {
        value: 'Anguilla',
        data: 'AI'
    }, {
        value: 'Antarctica',
        data: 'AQ'
    }, {
        value: 'Antigua And Barbuda',
        data: 'AG'
    }, {
        value: 'Argentina',
        data: 'AR'
    }, {
        value: 'Armenia',
        data: 'AM'
    }, {
        value: 'Aruba',
        data: 'AW'
    }, {
        value: 'Australia',
        data: 'AU'
    }, {
        value: 'Austria',
        data: 'AT'
    }, {
        value: 'Azerbaijan',
        data: 'AZ'
    }, {
        value: 'Bahamas',
        data: 'BS'
    }, {
        value: 'Bahrain',
        data: 'BH'
    }, {
        value: 'Bangladesh',
        data: 'BD'
    }, {
        value: 'Barbados',
        data: 'BB'
    }, {
        value: 'Belarus',
        data: 'BY'
    }, {
        value: 'Belgium',
        data: 'BE'
    }, {
        value: 'Belize',
        data: 'BZ'
    }, {
        value: 'Benin',
        data: 'BJ'
    }, {
        value: 'Bermuda',
        data: 'BM'
    }, {
        value: 'Bhutan',
        data: 'BT'
    }, {
        value: 'Bolivia',
        data: 'BO'
    }, {
        value: 'Bosnia And Herzegovina',
        data: 'BA'
    }, {
        value: 'Botswana',
        data: 'BW'
    }, {
        value: 'Bouvet Island',
        data: 'BV'
    }, {
        value: 'Brazil',
        data: 'BR'
    }, {
        value: 'British Indian Ocean Territory',
        data: 'IO'
    }, {
        value: 'Brunei Darussalam',
        data: 'BN'
    }, {
        value: 'Bulgaria',
        data: 'BG'
    }, {
        value: 'Burkina Faso',
        data: 'BF'
    }, {
        value: 'Burundi',
        data: 'BI'
    }, {
        value: 'Cambodia',
        data: 'KH'
    }, {
        value: 'Cameroon',
        data: 'CM'
    }, {
        value: 'Canada',
        data: 'CA'
    }, {
        value: 'Cape Verde',
        data: 'CV'
    }, {
        value: 'Cayman Islands',
        data: 'KY'
    }, {
        value: 'Central African Republic',
        data: 'CF'
    }, {
        value: 'Chad',
        data: 'TD'
    }, {
        value: 'Chile',
        data: 'CL'
    }, {
        value: 'China',
        data: 'CN'
    }, {
        value: 'Christmas Island',
        data: 'CX'
    }, {
        value: 'Cocos (Keeling) Islands',
        data: 'CC'
    }, {
        value: 'Colombia',
        data: 'CO'
    }, {
        value: 'Comoros',
        data: 'KM'
    }, {
        value: 'Congo',
        data: 'CG'
    }, {
        value: 'Congo, Democratic Republic',
        data: 'CD'
    }, {
        value: 'Cook Islands',
        data: 'CK'
    }, {
        value: 'Costa Rica',
        data: 'CR'
    }, {
        value: 'Cote D\'Ivoire',
        data: 'CI'
    }, {
        value: 'Croatia',
        data: 'HR'
    }, {
        value: 'Cuba',
        data: 'CU'
    }, {
        value: 'Cyprus',
        data: 'CY'
    }, {
        value: 'Czech Republic',
        data: 'CZ'
    }, {
        value: 'Denmark',
        data: 'DK'
    }, {
        value: 'Djibouti',
        data: 'DJ'
    }, {
        value: 'Dominica',
        data: 'DM'
    }, {
        value: 'Dominican Republic',
        data: 'DO'
    }, {
        value: 'Ecuador',
        data: 'EC'
    }, {
        value: 'Egypt',
        data: 'EG'
    }, {
        value: 'El Salvador',
        data: 'SV'
    }, {
        value: 'Equatorial Guinea',
        data: 'GQ'
    }, {
        value: 'Eritrea',
        data: 'ER'
    }, {
        value: 'Estonia',
        data: 'EE'
    }, {
        value: 'Ethiopia',
        data: 'ET'
    }, {
        value: 'Falkland Islands (Malvinas)',
        data: 'FK'
    }, {
        value: 'Faroe Islands',
        data: 'FO'
    }, {
        value: 'Fiji',
        data: 'FJ'
    }, {
        value: 'Finland',
        data: 'FI'
    }, {
        value: 'France',
        data: 'FR'
    }, {
        value: 'French Guiana',
        data: 'GF'
    }, {
        value: 'French Polynesia',
        data: 'PF'
    }, {
        value: 'French Southern Territories',
        data: 'TF'
    }, {
        value: 'Gabon',
        data: 'GA'
    }, {
        value: 'Gambia',
        data: 'GM'
    }, {
        value: 'Georgia',
        data: 'GE'
    }, {
        value: 'Germany',
        data: 'DE'
    }, {
        value: 'Ghana',
        data: 'GH'
    }, {
        value: 'Gibraltar',
        data: 'GI'
    }, {
        value: 'Greece',
        data: 'GR'
    }, {
        value: 'Greenland',
        data: 'GL'
    }, {
        value: 'Grenada',
        data: 'GD'
    }, {
        value: 'Guadeloupe',
        data: 'GP'
    }, {
        value: 'Guam',
        data: 'GU'
    }, {
        value: 'Guatemala',
        data: 'GT'
    }, {
        value: 'Guernsey',
        data: 'GG'
    }, {
        value: 'Guinea',
        data: 'GN'
    }, {
        value: 'Guinea-Bissau',
        data: 'GW'
    }, {
        value: 'Guyana',
        data: 'GY'
    }, {
        value: 'Haiti',
        data: 'HT'
    }, {
        value: 'Heard Island & Mcdonald Islands',
        data: 'HM'
    }, {
        value: 'Holy See (Vatican City State)',
        data: 'VA'
    }, {
        value: 'Honduras',
        data: 'HN'
    }, {
        value: 'Hong Kong',
        data: 'HK'
    }, {
        value: 'Hungary',
        data: 'HU'
    }, {
        value: 'Iceland',
        data: 'IS'
    }, {
        value: 'India',
        data: 'IN'
    }, {
        value: 'Indonesia',
        data: 'ID'
    }, {
        value: 'Iran, Islamic Republic Of',
        data: 'IR'
    }, {
        value: 'Iraq',
        data: 'IQ'
    }, {
        value: 'Ireland',
        data: 'IE'
    }, {
        value: 'Isle Of Man',
        data: 'IM'
    }, {
        value: 'Israel',
        data: 'IL'
    }, {
        value: 'Italy',
        data: 'IT'
    }, {
        value: 'Jamaica',
        data: 'JM'
    }, {
        value: 'Japan',
        data: 'JP'
    }, {
        value: 'Jersey',
        data: 'JE'
    }, {
        value: 'Jordan',
        data: 'JO'
    }, {
        value: 'Kazakhstan',
        data: 'KZ'
    }, {
        value: 'Kenya',
        data: 'KE'
    }, {
        value: 'Kiribati',
        data: 'KI'
    }, {
        value: 'Korea',
        data: 'KR'
    }, {
        value: 'Kuwait',
        data: 'KW'
    }, {
        value: 'Kyrgyzstan',
        data: 'KG'
    }, {
        value: 'Lao People\'s Democratic Republic',
        data: 'LA'
    }, {
        value: 'Latvia',
        data: 'LV'
    }, {
        value: 'Lebanon',
        data: 'LB'
    }, {
        value: 'Lesotho',
        data: 'LS'
    }, {
        value: 'Liberia',
        data: 'LR'
    }, {
        value: 'Libyan Arab Jamahiriya',
        data: 'LY'
    }, {
        value: 'Liechtenstein',
        data: 'LI'
    }, {
        value: 'Lithuania',
        data: 'LT'
    }, {
        value: 'Luxembourg',
        data: 'LU'
    }, {
        value: 'Macao',
        data: 'MO'
    }, {
        value: 'Macedonia',
        data: 'MK'
    }, {
        value: 'Madagascar',
        data: 'MG'
    }, {
        value: 'Malawi',
        data: 'MW'
    }, {
        value: 'Malaysia',
        data: 'MY'
    }, {
        value: 'Maldives',
        data: 'MV'
    }, {
        value: 'Mali',
        data: 'ML'
    }, {
        value: 'Malta',
        data: 'MT'
    }, {
        value: 'Marshall Islands',
        data: 'MH'
    }, {
        value: 'Martinique',
        data: 'MQ'
    }, {
        value: 'Mauritania',
        data: 'MR'
    }, {
        value: 'Mauritius',
        data: 'MU'
    }, {
        value: 'Mayotte',
        data: 'YT'
    }, {
        value: 'Mexico',
        data: 'MX'
    }, {
        value: 'Micronesia, Federated States Of',
        data: 'FM'
    }, {
        value: 'Moldova',
        data: 'MD'
    }, {
        value: 'Monaco',
        data: 'MC'
    }, {
        value: 'Mongolia',
        data: 'MN'
    }, {
        value: 'Montenegro',
        data: 'ME'
    }, {
        value: 'Montserrat',
        data: 'MS'
    }, {
        value: 'Morocco',
        data: 'MA'
    }, {
        value: 'Mozambique',
        data: 'MZ'
    }, {
        value: 'Myanmar',
        data: 'MM'
    }, {
        value: 'Namibia',
        data: 'NA'
    }, {
        value: 'Nauru',
        data: 'NR'
    }, {
        value: 'Nepal',
        data: 'NP'
    }, {
        value: 'Netherlands',
        data: 'NL'
    }, {
        value: 'Netherlands Antilles',
        data: 'AN'
    }, {
        value: 'New Caledonia',
        data: 'NC'
    }, {
        value: 'New Zealand',
        data: 'NZ'
    }, {
        value: 'Nicaragua',
        data: 'NI'
    }, {
        value: 'Niger',
        data: 'NE'
    }, {
        value: 'Nigeria',
        data: 'NG'
    }, {
        value: 'Niue',
        data: 'NU'
    }, {
        value: 'Norfolk Island',
        data: 'NF'
    }, {
        value: 'Northern Mariana Islands',
        data: 'MP'
    }, {
        value: 'Norway',
        data: 'NO'
    }, {
        value: 'Oman',
        data: 'OM'
    }, {
        value: 'Pakistan',
        data: 'PK'
    }, {
        value: 'Palau',
        data: 'PW'
    }, {
        value: 'Palestinian Territory, Occupied',
        data: 'PS'
    }, {
        value: 'Panama',
        data: 'PA'
    }, {
        value: 'Papua New Guinea',
        data: 'PG'
    }, {
        value: 'Paraguay',
        data: 'PY'
    }, {
        value: 'Peru',
        data: 'PE'
    }, {
        value: 'Philippines',
        data: 'PH'
    }, {
        value: 'Pitcairn',
        data: 'PN'
    }, {
        value: 'Poland',
        data: 'PL'
    }, {
        value: 'Portugal',
        data: 'PT'
    }, {
        value: 'Puerto Rico',
        data: 'PR'
    }, {
        value: 'Qatar',
        data: 'QA'
    }, {
        value: 'Reunion',
        data: 'RE'
    }, {
        value: 'Romania',
        data: 'RO'
    }, {
        value: 'Russian Federation',
        data: 'RU'
    }, {
        value: 'Rwanda',
        data: 'RW'
    }, {
        value: 'Saint Barthelemy',
        data: 'BL'
    }, {
        value: 'Saint Helena',
        data: 'SH'
    }, {
        value: 'Saint Kitts And Nevis',
        data: 'KN'
    }, {
        value: 'Saint Lucia',
        data: 'LC'
    }, {
        value: 'Saint Martin',
        data: 'MF'
    }, {
        value: 'Saint Pierre And Miquelon',
        data: 'PM'
    }, {
        value: 'Saint Vincent And Grenadines',
        data: 'VC'
    }, {
        value: 'Samoa',
        data: 'WS'
    }, {
        value: 'San Marino',
        data: 'SM'
    }, {
        value: 'Sao Tome And Principe',
        data: 'ST'
    }, {
        value: 'Saudi Arabia',
        data: 'SA'
    }, {
        value: 'Senegal',
        data: 'SN'
    }, {
        value: 'Serbia',
        data: 'RS'
    }, {
        value: 'Seychelles',
        data: 'SC'
    }, {
        value: 'Sierra Leone',
        data: 'SL'
    }, {
        value: 'Singapore',
        data: 'SG'
    }, {
        value: 'Slovakia',
        data: 'SK'
    }, {
        value: 'Slovenia',
        data: 'SI'
    }, {
        value: 'Solomon Islands',
        data: 'SB'
    }, {
        value: 'Somalia',
        data: 'SO'
    }, {
        value: 'South Africa',
        data: 'ZA'
    }, {
        value: 'South Georgia And Sandwich Isl.',
        data: 'GS'
    }, {
        value: 'Spain',
        data: 'ES'
    }, {
        value: 'Sri Lanka',
        data: 'LK'
    }, {
        value: 'Sudan',
        data: 'SD'
    }, {
        value: 'Suriname',
        data: 'SR'
    }, {
        value: 'Svalbard And Jan Mayen',
        data: 'SJ'
    }, {
        value: 'Swaziland',
        data: 'SZ'
    }, {
        value: 'Sweden',
        data: 'SE'
    }, {
        value: 'Switzerland',
        data: 'CH'
    }, {
        value: 'Syrian Arab Republic',
        data: 'SY'
    }, {
        value: 'Taiwan',
        data: 'TW'
    }, {
        value: 'Tajikistan',
        data: 'TJ'
    }, {
        value: 'Tanzania',
        data: 'TZ'
    }, {
        value: 'Thailand',
        data: 'TH'
    }, {
        value: 'Timor-Leste',
        data: 'TL'
    }, {
        value: 'Togo',
        data: 'TG'
    }, {
        value: 'Tokelau',
        data: 'TK'
    }, {
        value: 'Tonga',
        data: 'TO'
    }, {
        value: 'Trinidad And Tobago',
        data: 'TT'
    }, {
        value: 'Tunisia',
        data: 'TN'
    }, {
        value: 'Turkey',
        data: 'TR'
    }, {
        value: 'Turkmenistan',
        data: 'TM'
    }, {
        value: 'Turks And Caicos Islands',
        data: 'TC'
    }, {
        value: 'Tuvalu',
        data: 'TV'
    }, {
        value: 'Uganda',
        data: 'UG'
    }, {
        value: 'Ukraine',
        data: 'UA'
    }, {
        value: 'United Arab Emirates',
        data: 'AE'
    }, {
        value: 'United Kingdom',
        data: 'GB'
    }, {
        value: 'United States',
        data: 'US'
    }, {
        value: 'United States Outlying Islands',
        data: 'UM'
    }, {
        value: 'Uruguay',
        data: 'UY'
    }, {
        value: 'Uzbekistan',
        data: 'UZ'
    }, {
        value: 'Vanuatu',
        data: 'VU'
    }, {
        value: 'Venezuela',
        data: 'VE'
    }, {
        value: 'Viet Nam',
        data: 'VN'
    }, {
        value: 'Virgin Islands, British',
        data: 'VG'
    }, {
        value: 'Virgin Islands, U.S.',
        data: 'VI'
    }, {
        value: 'Wallis And Futuna',
        data: 'WF'
    }, {
        value: 'Western Sahara',
        data: 'EH'
    }, {
        value: 'Yemen',
        data: 'YE'
    }, {
        value: 'Zambia',
        data: 'ZM'
    },{
        value: 'Zimbabwe',
        data: 'ZW'
    },
];