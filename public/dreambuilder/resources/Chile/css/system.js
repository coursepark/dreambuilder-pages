//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------

$(document).ready(function(){

    $("body").append("<div id='loading' style='display:none;text-align:center;'><img src='/partnerControls/images/ajax-loader2.gif' /></div>");
	//$("body").append("<div id='introPop'><p>SkillsonlineNS is a partnership between Bluedrop, CBDC & Nova Scotia's Department of Labour and Advanced Education that gives every resident of the province access to:</p><ul><li>Thousands of FREE online courses on the most important workplace topics to improve their skills and career prospects</li><li>A CoursePark Lifelong Learning Profile to share with colleagues and employers</li><li>Career development tips and various online communities for professionals</li><li>The ability to build their own courses and sell them to the world</li></ul><p>SkillsonlineNS also gives every Business and Not for Profit the ability to have a Training Solution for their staff at no cost. Contact CoursePark to learn how you can train your employees for <strong>FREE!</strong></p><h1>Please Select One of the Following</h1><form name='locForm' id='locForm'><table cellpadding='0' cellspacing='0'><tr><td valign='top'><input type='radio' name='locPick' id='locPick' value='NS'></td><td>I reside in Nova Scotia AND/OR I work for an organization which has most of their employees in the Province<br><span style='font-weight:normal;'>I have read and understand the Government of <a href=\"javascript:showDis();\">Nova Scotia privacy disclaimer</a></span>.</td></tr><tr><td valign='top'><input type='radio' name='locPick' id='locPick' value='NOTNS'></td><td>I do not fit this criteria but I would still like to see more of CoursePark</td></tr></table></form><p class='fButton'><a href=\"javascript:introPopFinish();\"><img src='/ns/images/finishbutton.jpg' /></a></p>");
	//$("#introPop").dialog({modal: true,width:'600px', title:"Gift of Learning powered by CoursePark"});	
	//$("body").append("<div id='disclaimer'><p>The user is advised that any personal information provided, as well as the user agreement, is between the user, CoursePark and Bluedrop Performance Learning Inc. Also be advised that this online program is administered as a 'cloud' application. As such any information entered on the site is not stored on a server in Canada. Personal information may be stored in other countries such as the USA, the UK, and Hong Kong. The Province of Nova Scotia is not collecting the personal information on this site. The user is further advised to read the terms of use, and the privacy statement, prior to use of this site.</p></div>");

	$("body").append("<div id='dialogBox' ></div>");
	// Assign button actions
	$("#bSignUp").click(function() {
		createAcct();
		
	}); 
	$("#bSignIn").click(function() {
		goLogin("","");
	});
	$("#bSignUpAlt").click(function() {
		countrySelect();
		
	}); 
	
	//$("#activateSignup").click(function() {
	//	$("#signupForm").dialog({modal: true});	
	//});
	//alert("Position: "+chkSEE.indexOf("se_sc_"));
	
	// $("#locForm input[type='locPick']:checked").val();
	
	//$("#form1").attr("action", "https://www.coursepark.com/user/auth/login/url_code?redirect_url=/learningnetwork/courses/index/id/" + network +"/?lang="+lang); 
	$("#form1").attr("action", "https://www.coursepark.com/user/auth/login/url_code?redirect_url=/mycoursepark/?lang="+lang);
});
function showNotice() {
		$( "#notice" ).dialog({modal: true,title: 'Certain Conditions Apply',width:'450px',closeText:'Close',buttons: { "Close": function() { $(this).dialog("close")}}});	
}
function showDis() {
		$( "#disclaimer" ).dialog({modal: true,width:'450px',closeText:'Close',buttons: { "Close": function() { $(this).dialog("close")}}});	
}
function introPopFinish() {
	actVal = $('input[name=locPick]:checked').val(); 

	if (actVal == "NS") {
		$("#introPop").dialog("close");	
	} 
	if (actVal == "NOTNS") {
		document.location.href="http://www.coursepark.com";	
	}
	else {
			
	}
}

var itemHold;
var sel;
var source;
var countrySelect;
var userID;

function networkPicker(who) {
		if (who == "CH") {
			 return 2404;	
		}
		if (who == "PE") {
			return 2403;
		}	
}

function createAcct() {
	source = "DreamBuilder";
	//alert("clicked");
	
	var chkCode = "";
	//Gather values from form -------------------------------------------------------------------------
	em = $("#sEmail").val();
	fn = $("#fName").val();
	ln = $("#lName").val();
	pa = $("#sPass").val();
	country = country;
	pa2 = $("#password_confirm").val();
	tc = $('#course_park_tc').is(':checked');
	

	
	//if ($('#owner').is(':checked')) {
//		source = $('#owner').val();	
//	}
	//alert("Source: "+source);
//
//hld += "&email=gnb"+ document.getElementById("nID").value + "@coursepark.com";
//
	//Validate form values -------------------------------------------------------------------------
	if(validateCPForm()) {
		em = encodeURIComponent(em);
		//alert(em);
		  $("#loading").dialog({modal: true});	
		
	//assemble params to pass -------------------------------------------------------------------------
	hld = "firstname="+fn;
	hld += "&lastname="+ln;
	hld += "&email="+ em;
	hld += "&password="+pa;
	hld += "&activate="+activate;
	hld += "&country=" + country;
	hld += "&act=reg";
	hld += "&network=" + network;
	hld += "&live=" +live;
	hld += "&sel="; 
	hld += "&lang="+lang;
	hld += "&key=" + key;
	hld += "&sec=" + sec;

	hld += "&source=" + source;
	// ----------- SPECIAL EMAIL CONTROL ----------------------------------------------
	
	
	
	// -----------
	//Do loading call -------------------------------------------------------------------------
	$.ajax({
		type: "POST",
		url: "partnerControls/loader.php?"+hld,
		context: document.body,
		data: hld,
		dataType:"json",
	error:function (xhr, ajaxOptions, thrownError){
			   alert("We're sorry, your login could not be processed at this time."+thrownError);
        },
		success: function(data){
			itemHold = data;
			chkCode = itemHold.status.code;	
			chkMessage = itemHold.status.message;
			//userID = itemHold.data.id;
			switch (chkCode) {
				case 200:
				//em = decodeURIComponent(em);
				
				if (mailEnable == 1) {
					var mailer = $.ajax({
							url:"partnerControls/loader.php?act=mail&lang="+lang+"&email="+em+"&from=" + emailFrom,
							type:"get",
							success: function() {
								$( "#loading" ).dialog("close");	
								em = decodeURIComponent(em);
								goLogin(em,pa);		
							}
							});
				} else {
					
				//	$("#form1").attr("action", "https://www.coursepark.com/user/auth/login/url_code?redirect_url=/learningnetwork/courses/index/id/" + network +"/tab/tab_allCourses/?lang="+lang); 
				if (network == 2402) {
							$("#form1").attr("action", "https://www.coursepark.com/user/auth/login/url_code?redirect_url=/learningnetwork/courses/index/id/" + network +"/tab/tab_allCourses/?lang="+lang); 
						} else {
							$("#form1").attr("action", "https://www.coursepark.com/user/auth/login/url_code?redirect_url=/learningnetwork/courses/index/id/" + network +"/tab/tab_allCertifications/?lang="+lang); 
						}
						if (activate ==1) {
						//goLogin(em,pa);		
						userID = itemHold.data.id;
						doubleJoiner(userID,em,pa,fn,ln);
					} else {
						goMessage("confirmmessage",chkMessage);
					}
					//$( "#loading" ).dialog("close");	
					//
					
				}
					 break;
				case 400:
				$( "#loading" ).dialog("close");	
					goMessage("custom",chkMessage);
					break;
				default:
				$( "#loading" ).dialog("close");	
					goMessage("createFail");
					break;
			}			
			},
		complete: function(data) {
			}
	});
	}
	
	} 

function validateCPForm() {
	//alert("TC: "+tc);
	if (!validateEmail(em)) {
		goMessage("email");
		return false;
	} else {
		if (fn == '' || ln == '' || pa == '') {
			goMessage("allfields");
			return false;
		} else {
			if (pa != pa2) {
				goMessage("passfail");	
				return false;
			} else {
				if (tc == false) {
				  goMessage("terms");
					return false;	
				} else {
					if (pa.length < 6) {
						goMessage("tooshort");	
					} else {
						
							return true	
						
					}
				}
			}
		}
}
}
function goMessage(which,message) {
	switch (lang) {
		case "en":
	switch (which) {
		case "email":
			what = "Please enter a valid email address.";
			break;
		case "allfields":
			what = "Please complete all fields.";
			break;
		case "passfail":
			what = "Passwords do not match.";
			break;
		case "terms":
			what = "Please agree to the Terms and Conditions of CoursePark to continue";
			break;
		case "createFail":
			what = "Your account could not be created at this time.";
			break;
		case "exists":
			what = "An account with this Employee ID address already exists.";
			break;
		case "tooshort":
			what = "Password length must be 6 characters or more";
			break;
		case "userInvalid":
			what = "Your account could not be verified, please check your Email and Password and try again.";
			break;
		case "nonprofit":
			what = "Please certify that you are representing a Not For Profit organization before proceeding";
			break;
		case "confirmmessage":
			what = "A verification email has been sent to the address you provided. Please click the link in the email to proceed to the DreamBuilder Learning Network";
			break;
		case "emailonlyMessage":
			what = "Your information has been sent to the Learning Network administrator. You will be notified when your registration has been complete. ";
			break;
		case "country":
			what = "Please select your country before continuing.";
			break;
		case "custom":
			what = message;
			break;
	}
		break;
		case "es":
	switch (which) {
		case "email":
			what = "Por favor ingrese una direccion de email valido.";
			break;
		case "allfields":
			what = "Por favor llene todos los espacios.";
			break;
		case "passfail":
			what = "Las contraseñas no son iguales.";
			break;
		case "terms":
			what = "Por favor acepte los Terminos y Condiciones de CoursePark para continuar.";
			break;
		case "createFail":
			what = "Su cuenta no puede ser creada en este momento.";
			break;
		case "exists":
			what = "Ya existe una cuenta con esta Identificacion.";
			break;
		case "tooshort":
			what = "La contraseña debe ser por lo menos de 6 carácteres.";
			break;
		case "userInvalid":
			what = "Su cuenta no pudo ser verificada. Por favor revise su Email y contraseña e intente de nuevo.";
			break;
		case "nonprofit":
			what = "Por favor certifique que ud. representa a una Organizacion No Gubermental antes de continuar.";
			break;
		case "confirmmessage":
			what = "Un email de verificación ha sido mando a la dirección provista.  Por favor pinche el link en el email para continuar e ingresar a la pagina de DreamBuilder Network de Aprendizaje.";
			break;
		case "emailonlyMessage":
			what = "Your information has been sent to the Learning Network administrator. You will be notified when your registration has been complete. ";
			break;
		case "country":
			what = "Por favor seleccione su país antes de continuar.";
			break;
		case "custom":
			what = message;
			break;
	}
			break;
	}
	
	$("#dialogBox").html(what);
		$( "#dialogBox" ).dialog({modal: true,closeText:'Close',buttons: { "Ok": function() { $(this).dialog("close")}}});	
}

function goLogin(em,pa) {
	if (em == "" && pa == "") {
			//em = 	encodeURIComponent($("#email").val());
			em = 	encodeURIComponent($("#email").val());
			pa = $("#password").val();
	
		}
	
	if (validateEmail(decodeURIComponent(em))) {
		if (pa != "") {
		
		x = decodeURIComponent(em);
		
		$( "#loading" ).dialog({modal: true});	
		
		$("#email").val(decodeURIComponent(em));
	//	$("#email").val(em);
		$("#password").val(pa);
		hld = "email=" +em;
		hld += "&password="+pa;
		hld += "&sel=exist";
		hld += "&act=reg";
		hld += "&key=" + key;
		hld += "&sec=" + sec;
		$("#loading").dialog({modal: true});	
		$.ajax({
			
		 url: "partnerControls/loader.php?"+hld,
		  type: "POST",
		  data: hld,
		  dataType:"json",
		  success: function(data){
			  itemHold = data;
				//itemHold = jQuery.parseJSON(itemHold);
				chkCode = itemHold.data.authenticated;	
				//alert("CODE: "+chkCode);
				  
				switch (chkCode) {
					
					case true:
					 $( "#loading" ).dialog("close");		
					 		//	$("#form1").attr("action", "https://www.coursepark.com/user/auth/login/url_code?redirect_url=/learningnetwork/courses/index/id/" + network +"/tab/tab_allCourses/?lang="+lang); 
				if (network == 2402) {
							//$("#form1").attr("action", "https://www.coursepark.com/user/auth/login/url_code?redirect_url=/learningnetwork/courses/index/id/" + network +"/tab/tab_allCourses/?lang="+lang); 
						} else { 
							//$("#form1").attr("action", "https://www.coursepark.com/user/auth/login/url_code?redirect_url=/learningnetwork/courses/index/id/" + network +"/tab/tab_allCertifications/?lang="+lang); 
						}
						 $("#form1").submit();
						break;
			default:
						$("#loading" ).dialog("close");	
						 goMessage("userInvalid");
						 break;l
				  }
		  }, 
		});
		} else {
			goMessage("allFields");	
		}
	} else {
		$("#loading" ).dialog("close");	
		goMessage("email");	
	}
}
function validateEmail(elementValue){  
    var emailPattern = /^[a-zA-Z0-9.\_\-\+]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;  
    return emailPattern.test(elementValue);  
 }

function countrySelect() {
	
	hld = $("#countrySelect").val();
	if (hld != "") {
		switch (hld) {
			case "Peru":
				network = 2733;
				break;	
			case "Chile":
				network = 2734;
				break;
			default:
				network = 2732;
				break;
		}
			createAcct();
	} else {
			goMessage("country");
	}

}
function doubleJoiner(who,em,pa,fn,ln) {
	$("#loading").dialog({modal: true});	
	hld = "network="+2728+"&userID="+who+"&act=reg&sel=joiner&email="+em+"&password="+pa+"&firstname="+fn+"&lastname="+ln;
		$.ajax({
			
		 url: "partnerControls/loader.php?"+hld,
		  type: "POST",
		  data: hld,
		  dataType:"json",
		  success: function(data){
			  itemHold = data;
				//itemHold = jQuery.parseJSON(itemHold);
				chkCode = itemHold.data;	
				//alert("CODE: "+chkCode);
				  
				switch (chkCode) {
					
					case true:
					 $( "#loading" ).dialog("close");		
					 	if (activate ==1) {
						goLogin(em,pa);		
					} else {
						goMessage("confirmmessage",chkMessage);
					}
						//$("#form1").attr("action", "https://www.coursepark.com/user/auth/login/url_code?redirect_url=/learningnetwork/courses/index/id/" + network +"/tab/tab_allCourses/?lang="+lang); 
						 //$("#form1").submit();
						// goLogin(em,pa);		
						break;
			default:
						$("#loading" ).dialog("close");	
						 goMessage("userInvalid");
						 break;l
				  }
		  }, 
		});
}