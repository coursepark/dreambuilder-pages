var API_URL, API_V2_URL;

if (window.location.href.match(/qc[\-\.]/) || window.location.href.match(/bluedrop\.bluedrop/)) {
	API_URL = 'https://kube-qc.bluedrop360.com/api-v1';
	API_V2_URL = 'https://kube-qc.bluedrop360.com/api-v2';
} else if (window.location.href.match(/support[\-\.]/)) {
	API_URL = 'https://kube-support.bluedrop360.com/api-v1';
	API_V2_URL = 'https://kube-support.bluedrop360.com/api-v2';
} else { 
	API_URL = 'https://kube-prod.bluedrop360.com/api-v1';
	API_V2_URL = 'https://kube-prod.bluedrop360.com/api-v2';
}

var User = {
	get: function(token) {
		return $.ajax({
			method: "GET",
			cache: false,
			url: API_URL + '/users/',
			headers: {
				'Authorization': token,
				'Content-Type': 'application/json'
			}
		})
	}
};
var Question = {
	get: function(userId, token) {
		return $.ajax({
			method: "GET",
			cache: false,
			url: API_V2_URL + '/courseware-question-answers?userId=' + userId,
			headers: {
				'Authorization': token,
				'Content-Type': 'application/json'
			}
		})
	},
	add: function(userId, token, questionKey, answer) {
		return $.ajax({
			method: "POST",
			url: API_V2_URL + '/courseware-question-answers',
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			headers: {
				'Authorization': token,
				'Content-Type': 'application/json'
			},
			data: JSON.stringify({
				'userId': userId,
				'questionKey': questionKey,
				'answer': answer
			}),
			success: function(res) {
				console.log("res", res);
			}
		})
	},
	edit: function(userId, token, questionKey, answer) {
		return $.ajax({
			method: "PUT",
			url: API_V2_URL + '/courseware-question-answers/?userId=' + userId + '&questionKey=' + questionKey,
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			headers: {
				'Authorization': token,
				'Content-Type': 'application/json'
			},
			data: JSON.stringify({
				'answer': answer
			})
		})
	},
	save: function(userId, token, questionKey, answer) {
		return Question.get(userId, token).then(
			function(res) {
				if (_.find(res,
						function(o) {
							return o.questionKey === questionKey;
						})) {
					return Question.edit(userId, token, questionKey, answer);
				} else {
					return Question.add(userId, token, questionKey, answer);
				}
			}
		);
	}
};
