'use strict';

module.exports = exports = function () {
	var express = require('express');
	var fs = require('fs');
	var nodefn = require('when/node');
	var path = require('path');
	var vhost = require('vhost');
	var when = require('when');
	
	var domainList = process.env.DOMAIN.split(',');
	var cacheControlMaxAge = process.env.CACHE_CONTROL_MAX_AGE || 0;
	var publicDir = path.join(__dirname, '..', 'public');
	var integrationsDir = path.join(__dirname, '..', 'integrations');
	
	var app = express();
	var media = express();
	
	media.use(function (req, res) {
		var domainPath = req.vhost.length ? req.vhost[0] : 'www';
		var reqPath = req.url;
		var index = 'index.html';
		
		if (reqPath.substr(-1) === '/') {
			// serve up the index page
			reqPath = path.join(reqPath, index);
		}
		
		if ((reqPath + '/').indexOf('/../') !== -1) {
			// don't allow creative file system access
			res.sendStatus(403);
			return;
		}
		
		var filePath = path.join(publicDir, domainPath, reqPath);
		
		when()
			.then(function () {
				// check for existence of requested file
				return nodefn.lift(fs.stat)(filePath);
			})
			.then(function (stat) {
				if (stat.isDirectory()) {
					throw 'directory redirect';
				}
				
				if (!stat.isFile()) {
					throw 404;
				}
			})
			.then(function () {
				res.set('Cache-Control', 'max-age=' + cacheControlMaxAge);
				res.sendFile(filePath);
			})
			.catch(function (err) {
				if (err === 'directory redirect') {
					// doesn't end with '/', redirect to that style of url
					console.log('directory redirect: ' + reqPath + ' -> ' + reqPath + '/');
					res.redirect(301, reqPath + '/');
					return;
				}
				
				// if file not found, always return index.html which will allow
				// single page applications to function
				var indexPath = path.join(publicDir, domainPath, index);
				nodefn
					.lift(fs.stat)(indexPath)
					.then(function (stat) {
						if (!stat || !stat.isFile()) {
							throw 404;
						}
						
						res.sendFile(indexPath);
					})
					.catch(function (err) {
						if (err === 404) {
							// subdomain directory doesn't exist or requested path/file
							// doesn't exist
							console.log('404: ' + filePath);
							res.sendStatus(404);
						}
						else {
							console.error(err);
							res.sendStatus(500);
						}
					})
				;
			})
			.done()
		;
	});
	
	function integrationIndex(subdomain) {
		return integrationsDir + '/' + subdomain + '/index.js';
	}
	
	// integrations
	nodefn.lift(fs.readdir)(integrationsDir)
		.then(function (dirs) {
			return when.filter(dirs, function (dir) {
				if (dir === '.') {
					// don't want the self reference
					return false;
				}
				
				var indexPath = integrationIndex(dir);
				return nodefn.lift(fs.stat)(indexPath).then(function (stat) {
					return stat.isFile();
				});
			});
		})
		.then(function (dirs) {
			for (var i = 0; i < domainList.length; i++) {
				var domain = domainList[i];
				for (var j = 0; j < dirs.length; j++) {
					var subdomain = dirs[j] + '.' + domain;
					var indexPath = integrationIndex(dirs[j]);
					app.use(vhost(subdomain, require(indexPath)));
				}
				
				// landing page work on any subdomain
				app.use(vhost('*.*.' + domain, media));
				app.use(vhost('*.' + domain, media));
				app.use(vhost(domain, media));
			}
		})
		.catch(console.error)
		.done()
	;
	
	return app;
};
