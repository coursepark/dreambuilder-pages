var app = require('./index')();
var port = process.env.PORT || 3000;

app.listen(port);

console.log('Listening at *.' + process.env.DOMAIN + ':' + port);

module.exports = exports = app;
