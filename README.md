# Bluedrop Learning Networks Landing Pages

[ ![Codeship Status for coursepark/bln-landing-pages](https://codeship.com/projects/fa7384f0-610a-0132-382e-1288075d9375/status?branch=master)](https://codeship.com/projects/51723)

Micro application for hosting custom organization landing pages for BLN.

## Requirements
- [node](http://nodejs.org)
- [npm](http://howtonode.org/introduction-to-npm)

## Setup
1. `cd` into root project directory
1. Run `npm install`

## Running
- Run `grunt server`

## Developing
1. Add a folder that matches the domain for an organization in `app/public/ORGANIZATION_NAME`
1. Add an entry to your `/etc/hosts` file for each organization like this: `127.0.0.1 ORGANIZATION_NAME.bln.local`
1. Visit `ORGANIZATION_NAME.bln.local:3000`

## Tests
- Run `npm test`
